function plan_to_g4mac_mono_en (plan, library, reduction_factor)

stderr = 2;

if nargin <= 2
	reduction_factor = 1;
end

% Define W as the width of the energy peak (MeV)
%W = 1;

% Define epsilon as the width of the rising or falling edge
%epsilon = 0.01;

%ions_list= {'H', 'He', 'Li', 'C', 'O', 'Ne'};
%ions_list = library

[xdim, ydim, zdim] = size (plan.target_volume);
half_x = xdim / 2;
half_y = ydim / 2;

%length(plan.ionlist)
%size(plan.beamgrid)

for ion = 1 : length (plan.ionlist)
	lib_idx = -1;
	
	for k = 1 : size (library, 2)
		if library{k}.name == plan.ionlist{ion}
			lib_idx = k;
			break;
		end
	end

%fprintf (stderr, 'ion# = %i (%s)\n', ion, plan.ionlist{ion});
	for pos_idx = 1 : size (plan.beamgrid, 1)
%fprintf (stderr, 'pos_idx = %i\n', pos_idx);
		x_coord = plan.beamgrid (pos_idx, 1);
		y_coord = plan.beamgrid (pos_idx, 2);
		
		en = plan.energies{pos_idx}{ion};
		weight = plan.weights{pos_idx}{ion};
		
%fprintf (stderr, 'Size of weights and energies: %i, %i'
		
		en = en (weight ~= 0);
		weight = weight (weight ~= 0);
		
		x_coord = x_coord - half_x;
		y_coord = y_coord - half_y;

		F1 = [plan.ionlist{ion}, '/'] + sprintf("P_%d_%d", x_coord, y_coord);

		if ~exist (F1, 'dir')
			mkdir(F1);
		end
% These are invariants (i.e. they are the same for all the energies at this position...).

		position_file = fopen (F1 + "/position.mac", 'w');
		fprintf (position_file, '/gps/pos/centre %.0f %.0f -150 mm\n', x_coord, y_coord);
		fclose (position_file);
		
		for en_idx = 1 : length (en)	
			F2 = F1 + sprintf ('/%d', en(en_idx));
			
%fprintf (stderr, 'F2 = %s\n', F2);

% Only create the folder if it doesn't exist
			if ~exist (F2, 'dir')
				mkdir(F2);
			end
			
			energy_file = fopen (F2 + '/en.mac', 'w');
			fprintf(energy_file, '# energy distribution\n');
			fprintf(energy_file,'/gps/ene/type Gauss\n');
			fprintf(energy_file,'/control/alias energyPerNucleon %.3f\n', en(en_idx));
			fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} %i\n', library{lib_idx}.A);
			
%			if plan.ionlist{ion} == 'H'
%				fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} 1\n');
%			elseif plan.ionlist{ion} == 'C'
%				fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} 12\n');
%			elseif plan.ionlist{ion} == 'He'
%				fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} 4\n');
%			elseif plan.ionlist{ion} == 'Li'
%				fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} 7\n');
%			elseif plan.ionlist{ion} == 'Fe'
%				fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} 56\n');
%			elseif plan.ionlist{ion} == 'O'
%				fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} 16\n');
%			elseif plan.ionlist{ion} == 'Ne'
%				fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} 20\n');
%			elseif plan.ionlist{ion} == 'Si'
%				fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} 28\n');
%			else
%				error ('Unknown ion\n');
%			end
			
			fprintf(energy_file,'/control/multiply spreadEnergy {totalEnergy} 0.002\n');
			fprintf(energy_file,'/gps/ene/mono {totalEnergy} MeV\n');
			fprintf(energy_file,'/gps/ene/sigma {spreadEnergy} MeV\n');

			fclose(energy_file);  

			n_primaries = weight (en_idx);
			number_of_primaries_file = fopen (F2 + '/primaries.mac', 'w');
			fprintf(number_of_primaries_file, '# number of primaries\n');
			fprintf(number_of_primaries_file,'/control/alias numberPrimaries %d\n', round (n_primaries / reduction_factor));
			fclose(number_of_primaries_file);
% Create a symoblic link for ion.mac and master run.mac
			system ("ln -sf ../position.mac ../../ion.mac ../../../run.mac " + F2);
		end
	end  
% pause;
end 
end
