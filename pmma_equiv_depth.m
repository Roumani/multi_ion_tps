function dprime = pmma_equiv_depth (material, t, T_by_value)

% Multiply by PMMA-equivalent range ratio

dprime = t * T_by_value (material).PERR;

% Water equivalent thickness is the thickness of water which will result in the same depth
% Water equivalent ratio is the ratio of thickness in water to thickness in material
% So for PMMA, WER = 1.157 meaning that depth in water is 1.157x depth in PMMA for same energy.
