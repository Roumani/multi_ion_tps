% Anything with Z less than or equal to this threshold will not be used in hypoxic regions
hypoxic_threshold_Z = 0;

% For multifield
%beam_angles = [0 45 90 135 180];
beam_angles = [0];

dose = 100;

% target, tail, distal, peripheral, proximal, OAR downsample factors
reg_decimations = [10, 10, 25, 200, 25, 10];

% increased this to 3 for speed
%reg_decimations = [3, 3, 25, 200, 25, 3, -1];

% Relative weightings given to the respective regions
reg_weights = [1, 1, 1, 1, 0.01, 10];

grid_x = 5;
grid_y = 5;
grid_z = 5;

% Create target dose distribution - sphere centred on the target volume

% Size of workspace
xpix = 450;
ypix = 100;
zpix = 450;

% Size of volume for actual optimisation
xrun = 100;
yrun = 100;
zrun = 300;

centrex = xpix / 2;
centrey = ypix / 2;
centrez = zpix / 2;

head_r = 100;
skin_t = 2;
skull_t = 6;

tumour_r = 25;
tumourx = 67;
tumoury = 0;
tumourz = 0;

oar_r = 10;
oarx = 67;
oary = 0;
oarz = 35;

hypoxic_r = 10;
hypoxicx = 52;
hypoxicy = 0;
hypoxicz = 0;

% Full set of ion data
ion_data_subset = ion_data(1);
