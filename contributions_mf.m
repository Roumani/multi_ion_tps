function [percent_contrib, total_percent_contrib] = contributions_mf (plan_list)

stdout = 1;

total_dv = 0;

nions = length (plan_list{1}.ionlist);
total_ion_dv = zeros (nions, 1);

for pidx = 1 : length (plan_list)
	split = split_plan (plan_list{pidx});

	fprintf (stdout, 'Field %i:\n', pidx);

	vtotal = 0;

	for n = 1 : length (split)
		vtotal = vtotal + split{n};
	end

	target_dose{pidx} = vtotal .* plan_list{pidx}.target_volume;

	dosevolume{pidx} = sum (target_dose{pidx}(:));
	total_dv = total_dv + dosevolume{pidx};

	for n = 1 : nions
		ion_dose = split{n} .* plan_list{pidx}.target_volume;
		ion_dosevolume = sum (ion_dose (:));
		total_ion_dv(n) = total_ion_dv(n) + ion_dosevolume;
		
		percent_contrib{pidx}(n) = ion_dosevolume / dosevolume{pidx} * 100;
		fprintf (stdout, '%s\t', plan_list{pidx}.ionlist{n});
	end

	fprintf (stdout, '\n');
	percent_contibution{pidx} = [];

	for n = 1 : nions
		fprintf (stdout, '%.2f\t', percent_contrib{pidx}(n));
		percent_contibution{pidx} = [percent_contibution{pidx} percent_contrib{pidx}(n)];
	end

	fprintf (stdout, '\n\n');
end

save ion_contribution.mat percent_contibution;

fprintf (stdout, '\n\nTotal:\n');

for n = 1 : nions
	fprintf (stdout, '%s\t', plan_list{1}.ionlist{n});
end

fprintf (stdout, '\n');

total_percent_contribution = [];

for n = 1 : nions
	total_percent_contrib(n) = total_ion_dv(n) / total_dv * 100;
	fprintf (stdout, '%.2f\t', total_percent_contrib(n));
	total_percent_contribution = [total_percent_contribution total_percent_contrib(n)];
end

save -ascii total_ion_contribution.mat total_percent_contribution;
