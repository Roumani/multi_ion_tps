function isosurf_anim (plan, V, filename, map, percentage)

if nargin < 4
	map = jet;
end

target_peak_dose = max (plan.target_volume (:));
Vmax = max (V(:));

thresh = percentage / 100 * target_peak_dose / Vmax; 
h = volshow (V, 'BackgroundColor', [0 0 0], 'Renderer', 'Isosurface', 'Isovalue', thresh, 'CameraPosition', campos, 'CameraUpVector', [1 0 0], 'ColorMap', map);

camproj ('perspective');
%camproj ('orthographic');

N = 500;

if nargin < 3
	filename = 'animation.gif';
end

vec = linspace(0, 4 * pi(), N)';
myPosition = 2 * [zeros(size(vec)) cos(vec) sin(vec)];

for idx = 1:N
% Update current view.
	h.CameraPosition = myPosition(idx, :);
% Use getframe to capture image.
	I = getframe(gcf);
	
	[indI, cm] = rgb2ind (I.cdata,256);
% Write frame to the GIF File.
	if idx == 1
		imwrite(indI, cm, filename, 'gif', 'Loopcount', inf, 'DelayTime', 0.08);
	else
		imwrite(indI, cm, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.08);
	end
end
