function plans_comparison(V1,V2)

% Find the difference between two two dose distribution volumes of two different plans

V_diff = V1-V2;



% Sum along Z

%Why is squeeze being used why not only sum(V_diff,3)??
A  = squeeze (sum(V_diff,3));

xproj = sum (A, 2);
yproj = sum (A, 1);

xmaxidx = (xproj == max (xproj));
ymaxidx = (yproj == max (yproj));


figure (1);

%subplot (3, 1, 1);
imagesc (squeeze (V_diff (xmaxidx, :, :)));
xlabel ('Z (depth) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
colorbar

title ('YZ slice, V1 - V2');

print ('YZ_V1-V2.eps', '-depsc2');
system ('epstopdf YZ_V1-V2.eps && pdftrimwhiteYZ_V1-V2.pdf');


figure(2);


imagesc (squeeze (V_diff (:, ymaxidx, :)));
xlabel ('Z (depth) (mm)');
ylabel ('X (horizontal) (mm)')
axis equal;
colorbar

title ('XZ slice, V1 -V2');

print ('XZ_V1-V2.eps', '-depsc2');
system ('epstopdf XZ_V1-V2.eps && pdftrimwhite XZ_V1-V2.pdf');