% Example usage: isodose (plan, V, -60, jet(256))

function isodose (plan, V, theta, map)

%cbar = (0:99) / 99 * max (V (:));

%V (:, 1, 1) = cbar;

target_peak_dose = max (plan.target_volume (:));
Vmax = max (V(:));

thresh_50 = 0.5 * target_peak_dose / Vmax; 
thresh_80 = 0.8 * target_peak_dose / Vmax; 
thresh_90 = 0.9 * target_peak_dose / Vmax; 
thresh_95 = 0.95 * target_peak_dose / Vmax; 

campos = [0.5, 1*cosd(theta) + 1, 1*sind(theta)] + [0 0 -0.1];
camtarg = -[0.5, 1*cosd(theta) + 1, 1*sind(theta)] + [0 0 -0.1];%[-0.5, -0.5, 0];

%h = volshow (V, 'BackgroundColor', [0 0 0], 'Renderer', 'MaximumIntensityProjection', 'CameraPosition', [1, 2*cosd(theta), 2*sind(theta)], 'CameraUpVector', [1 0 0], 'ColorMap', map);
h = volshow (V, 'BackgroundColor', [0 0 0], 'Renderer', 'Isosurface', 'Isovalue', thresh_50, 'CameraPosition', campos, 'CameraUpVector', [1 0 0], 'ColorMap', map, 'CameraTarget', camtarg);
I = getframe (gcf);
imwrite (I.cdata, 'isodose_50.png', 'png');

h = volshow (V, 'BackgroundColor', [0 0 0], 'Renderer', 'Isosurface', 'Isovalue', thresh_80, 'CameraPosition', campos, 'CameraUpVector', [1 0 0], 'ColorMap', map, 'CameraTarget', camtarg);
I = getframe (gcf);
imwrite (I.cdata, 'isodose_80.png', 'png');

h = volshow (V, 'BackgroundColor', [0 0 0], 'Renderer', 'Isosurface', 'Isovalue', thresh_90, 'CameraPosition', campos, 'CameraUpVector', [1 0 0], 'ColorMap', map, 'CameraTarget', camtarg);
I = getframe (gcf);
imwrite (I.cdata, 'isodose_90.png', 'png');

h = volshow (V, 'BackgroundColor', [0 0 0], 'Renderer', 'Isosurface', 'Isovalue', thresh_95, 'CameraPosition', campos, 'CameraUpVector', [1 0 0], 'ColorMap', map, 'CameraTarget', camtarg);
I = getframe (gcf);
imwrite (I.cdata, 'isodose_95.png', 'png');
