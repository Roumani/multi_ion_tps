function edeps_graypp = mev2graypp (edeps_mev, volume_m3, n_primary_particles)

% we have energy in MeV deposited in a 1 mm cubic voxel = 1e-9 m^3

J_per_MeV = 1.602e-19 * 1e6; % joules / MeV

edeps_graypp = edeps_mev * J_per_MeV / n_primary_particles / volume_m3;
