cd Proton

load p_fluence_100_150.mat;
load p_fluence_140_190.mat;

%load p100_150_energies.dat
%load p100_150_weights.dat
%load p140_190_energies.dat
%load p140_190_weights.dat

%fprintf (stderr, 'Calculating neutron fluence, proton beam, 100-150 mm\n');
%p_fluence_100_150 = calc_neutronfluence (p100_150_energies, p100_150_weights);
%fprintf (stderr, 'Calculating neutron fluence, proton beam, 140-190 mm\n');
%p_fluence_140_190 = calc_neutronfluence (p140_190_energies, p140_190_weights);

cd ../Carbon

load c_fluence_100_150.mat;
load c_fluence_140_190.mat;

%load c100_150_energies.dat
%load c100_150_weights.dat
%load c140_190_energies.dat
%load c140_190_weights.dat

%fprintf (stderr, 'Calculating neutron fluence, carbon beam, 100-150 mm\n');
%c_fluence_100_150 = calc_neutronfluence (c100_150_energies, c100_150_weights);
%fprintf (stderr, 'Calculating neutron fluence, carbon beam, 140-190 mm\n');
%c_fluence_140_190 = calc_neutronfluence (c140_190_energies, c140_190_weights);

cd ..

min_p_100_150 = min (min (min (p_fluence_100_150 (225:275, 225:275, 100:150))))
mean_p_100_150 = mean (mean (mean (p_fluence_100_150 (225:275, 225:275, 100:150))))
max_p_100_150 = max (max (max (p_fluence_100_150 (225:275, 225:275, 100:150))))

min_p_140_190 = min (min (min (p_fluence_140_190 (225:275, 225:275, 140:190))))
mean_p_140_190 = mean (mean (mean (p_fluence_140_190 (225:275, 225:275, 140:190))))
max_p_140_190 = max (max (max (p_fluence_140_190 (225:275, 225:275, 140:190))))

min_c_100_150 = min (min (min (c_fluence_100_150 (225:275, 225:275, 100:150))))
mean_c_100_150 = mean (mean (mean (c_fluence_100_150 (225:275, 225:275, 100:150))))
max_c_100_150 = max (max (max (c_fluence_100_150 (225:275, 225:275, 100:150))))

min_c_140_190 = min (min (min (c_fluence_140_190 (225:275, 225:275, 140:190))))
mean_c_140_190 = mean (mean (mean (c_fluence_140_190 (225:275, 225:275, 140:190))))
max_c_140_190 = max (max (max (c_fluence_140_190 (225:275, 225:275, 140:190))))

[B10_conc_ppm_p_100_150, Gd157_conc_ppm_p_100_150] = fluence2conc (mean_p_100_150)
[B10_conc_ppm_p_140_190, Gd157_conc_ppm_p_140_190] = fluence2conc (mean_p_140_190)
[B10_conc_ppm_c_100_150, Gd157_conc_ppm_c_100_150] = fluence2conc (mean_c_100_150)
[B10_conc_ppm_c_140_190, Gd157_conc_ppm_c_140_190] = fluence2conc (mean_c_140_190)
