% Provide a list of energies

function [final_energy_weights, weighted_dose, energies] = treatmentplan3 (energies, depth, width, height, target_dose)
% Load biological dose profiles for each of the proposed energies

%cmap = gray (256);
%cmap = cmap (end:-1:1, :);
cmap = jet (256);
contour_levels = 19;
lw = 2;
fontsize = 25;
x0 = 10;
y0 = 10;
imwidth = 3000;
imheight = 1150;

ne = length (energies);

if ne < 2
	error ('Need at least 2 energies');
end

tmp = load (sprintf ('planes_bio_edep_gyepp/%.0f.mat', energies (1)));

f = fieldnames (tmp);

d = getfield (tmp, f{1, 1});

[nx, ny, nz] = size (d);

components = zeros (nx, ny, nz, ne);

%flat_components = zeros (nz, ne);

components (:, :, :, 1) = d;

depths = zeros (ne, 1);
flattened = squeeze (sum (d));
[~, cmax] = find (flattened == max (max (flattened)));
depths (1) = round (mean (cmax));

for en = 2 : ne
	tmp = load (sprintf ('planes_bio_edep_gyepp/%.0f.mat', energies (en)));

	f = fieldnames (tmp);

	d = getfield (tmp, f{1, 1});

	flattened = squeeze (sum (d));
	[~, cmax] = find (flattened == max (max (flattened)));
	depths (en) = round (mean (cmax));

	components (:, :, :, en) = d;
end

qualified = (depths >= depth (1) & depths <= depth (2));

components = components (:, :, :, qualified);
energies = energies (qualified);
ne = max (size (energies));

tpv = components (floor (nx / 2) + (width(1) : width(2)), floor (ny / 2) + (height(1) : height(2)), :, :);

flat_components = squeeze (mean (mean (tpv)));

figure(1);
plot (flat_components);
drawnow;

print (sprintf ('unweighted_components_%i_%i.eps', depth (1), depth (2)), '-depsc2');

target = zeros (1, nz);

target (depth(1) : depth(2)) = target_dose;

[final_energy_weights, final] = fit_energies (flat_components', target);

RBE_depth = [0 20 30 40 50 60 70 100 105 110 115 120 130 140 150 160 250];
phys_dose = [2.3 2.32 2.35 2.4 2.45 2.55 2.8 2.85 2.75 2.75 2.65 2.5 2.4 2.2 1.9 0.6 0.5];
bio_dose = [2.6 2.62 2.7 2.8 2.95 3.15 3.6 4.05 4.05 4.05 4.05 4.05 4.05 4.05 4.05 0.95 0.7];
RBE = bio_dose ./ phys_dose;
RBE (1:6) = 1.3;
RBE (6:7) = [1.31 1.325];

RBE_ext = interp1 (RBE_depth, RBE, 0 : nz - 1, 'linear', 'extrap');

final_phys = final ./ RBE_ext;

fluence = zeros (1, nz);

depths = depths (qualified);

for d = 1 : size (depths)
	fluence (1 : depths (d)) = fluence (1 : depths (d)) + final_energy_weights (d);
	fluence (1 : depths (d)) = fluence (1 : depths (d)) - (0 : (depths (d) - 1)) * (0.3 * final_energy_weights (d) / 100);
end

fluence = fluence / max (fluence) * 100;

figure (2);
clf;
plot (0 : nz - 1, final, '-r', 0 : nz - 1, final_phys, '-b');
hold on;
[ax, h1, h2] = plotyy (0 : nz - 1, target, 0 : nz - 1, fluence);
grid on;
set (h1, 'linestyle', '-', 'color', 'k');
set (h2, 'linestyle', '--', 'color', 'm');
set (ax, 'ylabel', 'Normalised Primary Particle Fluence (%)');
xlabel ('Depth (mm)');
ylabel ('Photon-Equivalent Dose (GyE) and Dose (Gy)');
legend ('Biological Dose (GyE)', 'Physical Dose (Gy)', 'Target Biological Dose (GyE)', 'Normalised Primary Particle Fluence (%)');
drawnow;
hold off;
print (sprintf ('target_fit_%i_%i.eps', depth (1), depth (2)), '-depsc2');

weighted_dose = zeros (nx, ny, nz);

for en = 1 : ne
	weighted_dose = weighted_dose + components (:, :, :, en) * final_energy_weights (en);
end

save (sprintf ('weighted_dose_%i_%i.mat', depth(1), depth(2)), 'weighted_dose');

flattened = squeeze (mean (weighted_dose ((-25 : 25) + ceil (nx / 2), :, :), 1));

save (sprintf ('flattened_%i_%i.mat', depth(1), depth(2)), 'flattened');

yz = squeeze (weighted_dose (round (nx / 2), :, :));

save (sprintf ('yz_%i_%i.mat', depth(1), depth(2)), 'yz');

xy = squeeze (weighted_dose (:, :, round (nz / 2)));

save (sprintf ('xy_%i_%i.mat', depth(1), depth(2)), 'xy');

return;

s = yz;

[nr, nc] = size (flattened);

figure (3);
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), flattened);
axis equal;
colormap (cmap);
cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_yz_flattened_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

imwrite (flattened / max (max (flattened)) * 256, cmap, sprintf ('dosedist_yz_flattened_%i_%i.png', round (depth (1)), round (depth (2))));

figure(4);

set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s);
axis equal;
colormap (cmap);
cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_yz_centreslice_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

imwrite (s / max (max (s)) * 256, cmap, sprintf ('dosedist_yz_centreslice_%i_%i.png', round (depth (1)), round (depth (2))));

s2 = round (s * 99);
f2 = round (flattened * 99);
s2f = medfilt2 (s2, [5, 5]);
f2f = medfilt2 (f2, [5, 5]);
 
figure(5);
clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
%imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), f2);
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), f2f, contour_levels, 'LineWidth', lw);
clabel (c, h, 'FontSize', fontsize, 'Color', 'r');
axis equal;
colormap (cmap);
%cb = colorbar ('eastoutside');
drawnow;
hold off;
print (sprintf ('dosedist_contour_yz_flattened_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

figure(6);
clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
%imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s2);
s_max = max (max (s2))
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s2f, contour_levels, 'LineWidth', lw);
clabel (c, h, 'FontSize', fontsize, 'Color', 'r');
axis equal;
colormap (cmap);
%cb = colorbar ('eastoutside');
drawnow;
hold off;
print (sprintf ('dosedist_contour_yz_centreslice_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

flattened (flattened > target_dose) = target_dose;
s (s > target_dose) = target_dose;

f2 = round (flattened * 99);
s2 = round (s * 99);

figure (7);
clf;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), flattened);
axis equal;
colormap (cmap);
cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_yz_flattened_clamped_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

imwrite (flattened / max (max (flattened)) * 256, cmap, sprintf ('dosedist_yz_flattened_clamped_%i_%i.png', round (depth (1)), round (depth (2))));

figure(8);
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s);
axis equal;
colormap (cmap);
cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_yz_centreslice_clamped_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

imwrite (s / max (max (s)) * 256, cmap, sprintf ('dosedist_yz_centreslice_clamped_%i_%i.png', round (depth (1)), round (depth (2))));

figure(9);
clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
%imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s2);
s_max = max (max (s2))
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s2f, contour_levels, 'LineWidth', lw);
clabel (c, h, 'FontSize', fontsize, 'Color', 'r');
axis equal;
colormap (cmap);
%cb = colorbar ('eastoutside');
drawnow;
hold off;
print (sprintf ('dosedist_contour_yz_centreslice_clamped_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

figure(10);
clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
%imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), f2);
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), f2f, contour_levels, 'LineWidth', lw);
axis equal;
colormap (cmap);
%cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_contour_yz_flattened_clamped_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');
clabel (c, h, 'FontSize', fontsize, 'Color', 'r', 'labelspacing', 300);
print (sprintf ('dosedist_contour_yz_flattened_clamped_labelled_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');
hold off;

end
