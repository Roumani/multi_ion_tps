function split_energies (filename, energies, output_folder)

d = load (filename);

f = fieldnames (d);

d = getfield (d, f{1, 1});

dims = size (d);

ne = max (size (energies))

if ne ~= dims (end)
	error (sprintf ('Number of energies (%d) should be the same as the size of the last dimension of the data in %s (%d)', filename, max (size (energies)), dims (end)));
end

if ~exist (output_folder, 'dir')
	mkdir (output_folder);
end

for en = 1 : dims (end)
	data = d (:, :, :, en);
	save ('-v7.3', sprintf ('%s/%.0f.mat', output_folder, energies (en)), 'data')
end

end
