cd Proton
[p100_150_weights, p100_150_dose, p100_150_energies] = treatmentplan2 ([125:1:160], [100 150], [-25 25], [-25 25], 1);
[p140_190_weights, p140_190_dose, p140_190_energies] = treatmentplan2 ([153:1:182], [140 190], [-25 25], [-25 25], 1);
cd ../Carbon
[c100_150_weights, c100_150_dose, c100_150_energies] = treatmentplan2 ([239:2:304], [100 150], [-25 25], [-25 25], 1);
[c140_190_weights, c140_190_dose, c140_190_energies] = treatmentplan2 ([289:2:350], [140 190], [-25 25], [-25 25], 1);
cd ..
