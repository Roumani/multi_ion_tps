function phys2bioC12 (physical_folder, biological_folder)

RBE0 = 1.5;
RBE1 = 3;
thresh = 0.6;

physical_doses = dir (sprintf ('%s/*.dat', physical_folder));
physical_doses_filenames = {physical_doses.name};

if ~exist (biological_folder, 'dir')
	mkdir (biological_folder);
end

for pf = 1 : length (physical_doses_filenames)
	phys = load (sprintf ('%s/%s', physical_folder, physical_doses_filenames{pf}));
	f = fieldnames (phys);
	phys = getfield (phys, f {1, 1});

	flat = squeeze (sum (sum (phys, 1), 2));
	
	z = (find (flat > 0.9 * max (flat)));
	z_proximal = min (z);
	z_distal = max (z);

	pmax = max (max (max (phys)));
	[xm, ym, zm] = find (phys == pmax);
	
% This shit be cray.
% If we are in the entrance/buildup region (< 60% of max), we multiply by RBE = 1.5
% If we are at peak, we multiply by RBE = 3
% If we are somewhere in between... we need
	
	m = (RBE1 - RBE0) / ((1 - thresh) * pmax);
	b = RBE0 - (thresh * pmax) * m;

	bio = zeros (size (phys));

	r1 = phys < thresh * pmax;
	r2 = ~r1;
	
	bio (r1) = phys (r1) * RBE0;
	bio (r2) = phys (r2) .* (m * phys (r2) + b);
	
%	figure (1) ; imagesc ([(squeeze (sum (phys, 1))); (squeeze (sum (bio, 1)))]);
	
	save ('-binary', sprintf ('%s/%s', biological_folder, physical_doses_filenames{pf}), 'bio')
end
	
end
