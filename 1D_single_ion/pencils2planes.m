function pencils2planes (pencils_folder, planes_folder, x_offs, y_offs)

stderr = 2;
pencils = dir ([pencils_folder '/*.*at']);
pencils_filenames = {pencils.name};

if ~exist (planes_folder, 'dir')
	mkdir (planes_folder);
end

n_spots_x = max (size (x_offs));
n_spots_y = max (size (y_offs));

for pf = 1 : length (pencils_filenames)
	fprintf (stderr, '%s\n', pencils_filenames{pf});
	pencil = load (sprintf ('%s/%s', pencils_folder, pencils_filenames{pf}));
	f = fieldnames (pencil);
	pencil = getfield (pencil, f {1, 1});

	[nx, ny, nz] = size (pencil);

	nx2 = round (nx / 2);
	ny2 = round (ny / 2);

	plane = zeros (2 * nx + 1, 2 * ny + 1, nz);

	for x = x_offs
		for y = y_offs
%			fprintf (stderr, 'Beam scanning index (%d, %d)\n', x, y);
			plane (1 + nx + x + (-nx2 : (nx2 - 1)), 1 + ny + y + (-ny2 : (ny2 - 1)), :, :) = plane (1 + nx + x + (-nx2 : (nx2 - 1)), 1 + ny + y + (-ny2 : (ny2 - 1)), :, :) + pencil;
		end
	end

	plane = plane / (n_spots_x * n_spots_y);

	save (sprintf ('%s/%s', planes_folder, pencils_filenames{pf}), 'plane')
end

end

