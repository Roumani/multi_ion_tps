% Provide a list of energies

function [final_energy_weights, weighted_dose] = treatmentplan (energies, depth, width, height, target_dose)
% Load biological dose profiles for each of the proposed energies

ne = length (energies);

if ne < 2
	error ('Need at least 2 energies');
end

tmp = load (sprintf ('planes_bio_edep_gyepp/%.0f.dat', energies (1)));

f = fieldnames (tmp);

d = getfield (tmp, f{1, 1});

[nx, ny, nz] = size (d);

components = zeros (nx, ny, nz, ne);

%flat_components = zeros (nz, ne);

components (:, :, :, 1) = d;

for en = 2 : ne
	tmp = load (sprintf ('planes_bio_edep_gyepp/%.0f.dat', energies (en)));

	f = fieldnames (tmp);

	d = getfield (tmp, f{1, 1});

	components (:, :, :, en) = d;
end

%size (components);

tpv = components (floor (nx / 2) + (width(1) : width(2)), floor (ny / 2) + (height(1) : height(2)), :, :);

%size (tpv)

flat_components = squeeze (mean (mean (tpv)));

size (flat_components)

figure(1);
plot (flat_components);

target = zeros (1, nz);

target (depth(1) : depth(2)) = target_dose;

[final_energy_weights, final] = fit_energies (flat_components', target);

%size (target)
%size (final)

figure (2);
plot (0 : nz - 1, final, 0 : nz - 1, target);
grid on;
xlabel ('Depth (mm)');
ylabel ('Photon-Equivalent Dose (GyE)');
legend ('Fit', 'Target');

weighted_dose = zeros (nx, ny, nz);

for en = 1 : ne
	weighted_dose = weighted_dose + components (:, :, :, en) * final_energy_weights (en);
end

flattened = squeeze (sum (weighted_dose, 1));

[nr, nc] = size (flattened);

figure(3);
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), flattened);
axis equal;
colormap (jet);

end
