function do_neutron_plots (data, str, R1, R2);

[nx, nr, nc] = size (data)

x0 = 10;
y0 = 10;
width = 2100;
height = 2000;
fontsize = 25;

figure(1);
clf
set (gcf, 'units', 'points', 'position', [x0, y0, width, height]);
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), squeeze (data (round (nx / 2), :, :)));
a = gca;
set (a, 'FontSize', fontsize);
axis equal;
C = gray (4096);
%C = C (end:-1:1, :);
colormap (C);
cb = colorbar ('eastoutside');
rectangle ('Position', R1, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
drawnow;
print (sprintf ('ndist_yz_centreslice_%s.pdf', str), '-dpdf');

figure(2);
% Normalise
data = data - min (min (min (data(round (nx / 2), :, :))));
data = 100 * data / max (max (max (data(round (nx / 2), :, :))));

clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, width, height])
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), squeeze (data (round (nx / 2), :, :)));
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), squeeze (data (round (nx / 2), :, :)), 'w');
clabel (c, h, 'FontSize', fontsize, 'Color', 'w');
axis equal;
colormap (C);
cb = colorbar ('eastoutside');
rectangle ('Position', R1, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
a = gca;
set (a, 'FontSize', fontsize);
drawnow;
hold off;
print (sprintf ('ndist_contour_yz_centreslice_%s.pdf', str), '-dpdf');

figure(3);
clf;

set (gcf, 'units', 'points', 'position', [x0, y0, width, height]);
imagesc ((0 : nx - 1) - floor (nx / 2), (0 : nr - 1) - floor (nr / 2), squeeze (data (:, :, round (nc / 2))));
a = gca;
set (a, 'FontSize', fontsize);
axis equal;
colormap (C);
cb = colorbar ('eastoutside');
rectangle ('Position', R2, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
drawnow;
print (sprintf ('ndist_xy_centreslice_%s.pdf', str), '-dpdf');

figure(4);
% Normalise
data = data - min (min (min (data (:, :, round (nc / 2)))));
data = 100 * data / max (max (max (data (:, :, round (nc / 2)))));

clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, width, height])
imagesc ((0 : nx - 1) - floor (nx / 2), (0 : nr - 1) - floor (nr / 2), squeeze (data (:, :, round (nc / 2))));
[c, h] = contour ((0 : nx - 1) - floor (nx / 2), (0 : nr - 1) - floor (nr / 2), squeeze (data (:, :, round (nc / 2))), 'w');
clabel (c, h, 'FontSize', fontsize, 'Color', 'w');
axis equal;
colormap (C);
cb = colorbar ('eastoutside');
rectangle ('Position', R2, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
a = gca;
set (a, 'FontSize', fontsize);
drawnow;
hold off;
print (sprintf ('ndist_contour_xy_centreslice_%s.pdf', str), '-dpdf');

end

