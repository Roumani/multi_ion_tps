
%load p_fluence_100_150.mat;
%load p_fluence_140_190.mat;
%load c_fluence_100_150.mat;
%load c_fluence_140_190.mat;

close all;

%load p_fluence_100_150.mat;
load c_fluence_100_150.mat;

%do_neutron_plots (p_fluence_100_150 (125:375, 125:375, :), 'proton_100_150', [100, -25, 50, 50], [-25, -25, 50, 50]);
%do_plots (p_fluence_140_190 (125:375, 125:375, :), 'proton_140_190', [140, -25, 50, 50], [-25, -25, 50, 50]);
close all;
do_neutron_plots (c_fluence_100_150 (125:375, 125:375, :), 'carbon_100_150', [100, -25, 50, 50], [-25, -25, 50, 50]);
%do_plots (c_fluence_140_190 (125:375, 125:375, :), 'carbon_140_190', [140, -25, 50, 50], [-25, -25, 50, 50]);

