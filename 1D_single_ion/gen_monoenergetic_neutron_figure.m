function gen_monoenergetic_neutron_figure (energy)

d = load (sprintf ('neutron_interpolated/%d.mat', energy));
do_plots (d.truncflux (:, :, :) / 100, sprintf ('fluence_%d', energy), [100, -25, 50, 50], [-25, -25, 50, 50]);

end

%d = load (sprintf ('pencil_bio_edep_gyepp/%d.mat', energy));
%do_plots (dose_dist (125:375, 125:375, :) / 100, sprintf ('fluence_%d', energy), [100, -25, 50, 50], [-25, -25, 50, 50]);

function do_plots (data, str, R1, R2);

[nx, nr, nc] = size (data)

x0 = 10;
y0 = 10;
width = 2100;
height = 2000;
fontsize = 25;

figure(1);
set (gcf, 'units', 'points', 'position', [x0, y0, width, height]);
d1 = squeeze (data (round (nx / 2), :, :));
d2 = medfilt2 (d1);
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), d1);
a = gca;
set (a, 'FontSize', fontsize);
axis equal;
colormap (jet (256));
cb = colorbar ('eastoutside');
rectangle ('Position', R1, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
drawnow;
print (sprintf ('yz_centreslice_%s.eps', str), '-depsc2');

figure(2);
% Normalise
data = data - min (min (min (data)));
data = 100 * data / max (max (max (data)));

clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, width, height])
d1 = squeeze (data (round (nx / 2), :, :));
d2 = medfilt2 (d1, [5, 5]);
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), d1);
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), d2, 'w');
clabel (c, h, 'FontSize', fontsize, 'Color', 'w');
axis equal;
colormap (jet (256));
cb = colorbar ('eastoutside');
rectangle ('Position', R1, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
a = gca;
set (a, 'FontSize', fontsize);
drawnow;
hold off;
print (sprintf ('contour_yz_centreslice_%s.eps', str), '-depsc2');

figure(3);
d1 = squeeze (data (:, :, round (nc / 2)));
d2 = medfilt2 (d1, [5, 5]);
set (gcf, 'units', 'points', 'position', [x0, y0, width, height]);
imagesc ((0 : nx - 1) - floor (nx / 2), (0 : nr - 1) - floor (nr / 2), d1);
a = gca;
set (a, 'FontSize', fontsize);
axis equal;
colormap (jet (256));
cb = colorbar ('eastoutside');
rectangle ('Position', R2, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
drawnow;
print (sprintf ('xy_centreslice_%s.eps', str), '-depsc2');

figure(4);
% Normalise
data = data - min (min (min (data)));
data = 100 * data / max (max (max (data)));

clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, width, height])
d1 = squeeze (data (:, :, round (nc / 2)));
d2 = medfilt2 (d1, [5, 5]);
imagesc ((0 : nx - 1) - floor (nx / 2), (0 : nr - 1) - floor (nr / 2), d1);
[c, h] = contour ((0 : nx - 1) - floor (nx / 2), (0 : nr - 1) - floor (nr / 2), d2, 'w');
clabel (c, h, 'FontSize', fontsize, 'Color', 'w');
axis equal;
colormap (jet (256));
cb = colorbar ('eastoutside');
rectangle ('Position', R2, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
a = gca;
set (a, 'FontSize', fontsize);
drawnow;
hold off;
print (sprintf ('contour_xy_centreslice_%s.eps', str), '-depsc2');

end
