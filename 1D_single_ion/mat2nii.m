function mat2nii (filename)

tmp = load (filename);
f = fieldnames (tmp);

data = getfield (tmp, f{1, 1});

data = data / max (max (max (data)));

[nr, nc, nd] = size (data);

if (nr < 251)
	tmp = zeros (251, 251, 250);
	tmp (125 + (-50:50), 125 + (-50:50), :) = data;
	data = tmp;
elseif (nr > 251)
	data = data (250 + (-125:125), 250 + (-125:125), :);
end

binfname = regexprep (filename, '.mat$', '.bin')
ecatfname = regexprep (filename, '.mat$', '.v')
niifname = regexprep (filename, '.mat$', '.nii')

f = fopen (binfname, 'w');
fwrite (f, data, 'float32');
fclose (f);

system (sprintf ('flo2ecat %s %s 250 1 251 251', binfname, ecatfname));
system (sprintf ('ecat2nii %s', ecatfname));

end
