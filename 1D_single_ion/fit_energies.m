% components should be flattened (summed in x and y)
% target should also be flattened

function [final_energy_weights, final] = fit_energies (components, target)
%	options = optimset ('lsqnonlin');
	options = optimset ();
	options = optimset (options, 'MaxFunEvals', 4000, 'MaxIter', 1000, 'TolFun', 1e-20, 'TolX', 1e-20);

	initial_energy_weights = rand (size (components, 1), 1);
	min_weights = zeros (size (initial_energy_weights));
	
	treatment_zone = find (target > 0);
	
	start = min (treatment_zone);
	finish = max (treatment_zone);
%	finish = max (size (target));
	
	f = @(energy_weights) energyobjfunc (energy_weights, target (start : finish), components (:, start : finish));

	[final_energy_weights, resnorm, resid, exitflag, output, lambda, jacobian] = lsqnonlin (f, initial_energy_weights, min_weights, [], options);

	size (components);
	size (final_energy_weights);

	final = weighted_energy_profile (components, final_energy_weights);
end
