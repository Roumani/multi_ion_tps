function profile = weighted_energy_profile (components, weights)
	profile = sum (weights .* components, 1);
end
