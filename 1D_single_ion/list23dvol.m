function edep3d = list23dvol (edep)

x = unique (edep (:, 1));
y = unique (edep (:, 2));
z = unique (edep (:, 3));

xmin = min (x);
xmax = max (x);
dx = x(2) - x(1);
ymin = min (y);
ymax = max (y);
dy = y(2) - y(1);
zmin = min (z);
zmax = max (z);
dz = z(2) - z(1);

%nx = rows (x)
%ny = rows (y)
%nz = rows (z)

ix = round ((edep (:, 1) - xmin) / dx);
iy = round ((edep (:, 2) - ymin) / dy);
iz = round ((edep (:, 3) - zmin) / dz);

nx = 1 + max (ix);
ny = 1 + max (iy);
nz = 1 + max (iz);

edep3d = zeros (nx * ny * nz, 1);

idx = ix + iy * nx + iz * nx * ny + 1;

edep3d (idx) = edep (:, 4);

edep3d = reshape (edep3d, nx, ny, nz);
