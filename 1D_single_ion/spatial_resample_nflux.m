function spatial_resample_nflux (input_filename, output_filename)

f = fopen (input_filename);

flux = fread (f, 'float32');

fclose (f);

flux = reshape (flux, 127, 252, 252);

n1 = (0 : 124) / 124;
n2 = (0 : 249) / 249;

flux = flux (2 : (end - 1), 2 : (end - 1), 2 : (end - 1));

flux = interp1 (n1, flux, n2);

flux = permute (flux, [2, 3, 1]);

save ('-7', output_filename, 'flux');
