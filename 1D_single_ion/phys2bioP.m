function phys2bioP (physical_folder, biological_folder)

RBE = 1.1;

physical_doses = dir (sprintf ('%s/*.dat', physical_folder));
physical_doses_filenames = {physical_doses.name};

if ~exist (biological_folder, 'dir')
	mkdir (biological_folder);
end

for pf = 1 : length (physical_doses_filenames)
	phys = load (sprintf ('%s/%s', physical_folder, physical_doses_filenames{pf}));
	f = fieldnames (phys);
	phys = getfield (phys, f {1, 1});

	bio = phys * RBE;
	
%	figure (1) ; imagesc ([(squeeze (sum (phys, 1))); (squeeze (sum (bio, 1)))]);
	
	save ('-binary', sprintf ('%s/%s', biological_folder, physical_doses_filenames{pf}), 'bio')
end
	
end
