% dA in cm^2
% D in gray

function ndist = totaln (flux, D, dA, K, I, J)

stderr = 2;

[nz, nx, ny, nd] = size (flux);

mz = round (nz / 2)
mx = round (ny / 2)
my = round (ny / 2)

wi = range (I) + 1;
wj = range (J) + 1;
wk = range (K) + 1;

%ndist = zeros (wk, wi, wj);
ndist = zeros (nz, nx, ny);

i0 = min (I);
j0 = min (J);
k0 = min (K);

mi = round (wi / 2);
mj = round (wj / 2);
mk = round (wk / 2);

if length (D) == 1
	D = D * ones (nz, nx, ny);
end

% flux (i, j, k)

tic

for k1 = K
% This is the flux which is active at this depth, with beam centred at i = mx, j = mx (which equates to x = 0, y = 0);
	aflux = squeeze (flux (:, :, :, k1 - k0 + 1));
	BPD = k1 * 2;
	fprintf (stderr, 'Irradiating slice %d, BP depth = %d\n', k1, BPD);

	for i1 = I
		for j1 = J
% Beam is targeted at k1, i1, j1
% Now iterate through all points in target volume & add flux
			dose = D (k1, i1, j1);

			for k2 = K
				for i2 = I
					for j2 = J
						ndist (k2, i2, j2) = ndist (k2, i2, j2) + aflux (k2, i2 - i1 + mx + 1, j2 - j1 + my + 1) * dA * dose;
					end
				end
			end
		end
	end
end

ndist = ndist (K, I, J);

%visndist (ndist, K, I, J);	
toc
