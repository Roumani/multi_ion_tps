% Arguments: fluence in n/cm^2, targ_x/y/z are voxel ranges, targ_conc is ppm boron, contrast_ratio = tumour:healthy
function dose = fluence2dose (fluence, targ_x, targ_y, targ_z, targ_conc, contrast_ratio)
	fluence2kerma = 8.66e-14;
	cbe_tumour = 3.8;
	cbe_healthy = 1.3;
	dose = fluence * fluence2kerma * cbe_healthy * targ_conc / contrast_ratio;
	dose (targ_x, targ_y, targ_z) = fluence (targ_x, targ_y, targ_z) * fluence2kerma * cbe_tumour * targ_conc;
end
