function [B10_result, Gd157_result] = fluence2conc (fluence_per_gray)
	B10_fluence2kerma = 8.66e-14;
	Gd157_fluence2kerma = 9.2e-15;
	
	B10_cbe_tumour = [3.8 1.2 9.94 4.22];
	B10_cbe_healthy = [1.3 0.37 4.25 0.94];
	B10_contrast = [5 1.2 2.8 0.3];
	
	B10_descriptor = {'B10 BPA, Capella, Corderra et al.', 'B10 BSH Morris et al, 2000', 'B10 BPA liver Suzuki 2000', 'B10 BSH liver Suzuki 2000'}
%	B10_cbe_healthy = 1.3;
	Gd157_cbe_tumour = [10 5 20 12.6 6 1.5];
% This is fake but will do for now until we get some meaningful values
	Gd157_cbe_healthy = [10 5 20 12.6 6 1.5];
	Gd157_contrast = [70 70 70 70 70 70]; % Cerrulo
	Gd157_descriptor = {'Humm et al., electrostatically bound to DNA', 'Fairlie et al., cells', 'Fairlie et al., nucleus', 'Cerullo et al., inside cylinder', 'Cerullo et al., surface of cylinder', 'Cerullo et al., outside cylinder'};
%	Gd10_cbe_healthy = ???;
		
	B10_conc_ppm = 0.1 ./ (fluence_per_gray * B10_fluence2kerma * B10_cbe_tumour);
	Gd157_conc_ppm = 0.1 ./ (fluence_per_gray * Gd157_fluence2kerma * Gd157_cbe_tumour);

% Work out what concentration we'd need in healthy tissues for 10% increase
	B10_healthy_conc_ppm = 0.1 ./ (fluence_per_gray * B10_fluence2kerma * B10_cbe_healthy);
	Gd157_healthy_conc_ppm = 0.1 ./ (fluence_per_gray * Gd157_fluence2kerma * Gd157_cbe_healthy);
	
% As the % increase in dose scales proportionally, we can calculate it based on the above figures (I think)
% 10 because above figures are for 10% (10x10%; hence increase in healthy tissue assuming 1:1 ratio)

	B10_result.conc_ppm = B10_conc_ppm;
	B10_result.descriptor = B10_descriptor;
	B10_result.B10_healthy_pct_increase = 10 * B10_conc_ppm ./ B10_healthy_conc_ppm ./ B10_contrast;

	Gd157_result.conc_ppm = Gd157_conc_ppm;
	Gd157_result.descriptor = Gd157_descriptor;
	Gd157_result.Gd157_healthy_pct_increase = 10 * Gd157_conc_ppm ./ Gd157_healthy_conc_ppm ./ Gd157_contrast;
end
