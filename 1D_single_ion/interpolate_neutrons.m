% neutron_energies is a vector listing the energies in MeV/u
% neutron_files is a cell array of the filenames corresponding to the above energies. Should be the same length!
% resampled_energies is the energies (MeV/u) at which we will interpolate

function interpolate_neutrons (neutron_energies, neutron_files, resampled_energies, output_folder)

stderr = 2;

ne = max (size (neutron_energies));
n_neutron_files = max (size (neutron_files));

if ne ~= n_neutron_files
	error ('Number of supplied energies (MeV/u) should match number of filenames listed in neutron_files');
end

e0 = load (neutron_files{1, end});

f = fieldnames (e0);

%e03d = permute (getfield (e0, f{1, 1}), [2, 3, 1]);
e03d = getfield (e0, f{1, 1});

%e03d = list23dvol (e0);
[nx, ny, nz] = size (e03d)

shifted_maps = zeros (nx, ny, nz * 1.2, max (size (neutron_energies)));

shifted_maps (:, :, 1:nz, end) = e03d;

flattened = squeeze (sum (e03d, 1));

figure (1); imagesc (flattened); drawnow;

[rmax, cmax] = find (flattened == max (max (flattened)));

cmax_ref = round (mean (cmax));

fprintf (stderr, 'Peak index of reference image: %d\n', cmax_ref)

offsets = zeros (ne, 1);

for n = 1 : (ne - 1)
	neutron_field = load (neutron_files{1, n});

	f = fieldnames (neutron_field);

%	neutron3d = permute (getfield (neutron_field, f{1, 1}), [2, 3, 1]);
	neutron3d = getfield (neutron_field, f{1, 1});

	flattened = squeeze (sum (neutron3d, 1));
	
	figure (2); imagesc (flattened);
	
	[rmax, cmax] = find (flattened == max (max (flattened)));
	cmax = round (mean (cmax));
	fprintf (stderr, 'Peak index of test image: %d\n', cmax)

%	continue;
	offsets (n) = cmax_ref - cmax;
	
	fprintf (stderr, 'Shifting by %d voxels\n', offsets (n));
	
	shifted_maps (:, :, offsets (n) + (1 : nz), n) = neutron3d (:, :, :);

%	for k = 1 : offsets (n)
%		shifted_maps (:, :, k, n) = neutron3d (:, :, 1);
%	end

	figure (3); subplot (ne, 1, n); imagesc (squeeze (sum (shifted_maps (:, :, :, n), 1)));
	drawnow;
end

%return;

figure (3); subplot (ne, 1, ne); imagesc (squeeze (sum (shifted_maps (:, :, :, ne), 1)));

%flux = zeros (nx, ny, nz * 2, nne);

%resampled_energies = min (neutron_energies) : max (neutron_energies);

interp_offsets = round (interp1 (neutron_energies, offsets, resampled_energies));

if ~exist (output_folder, 'dir')
	mkdir (output_folder);
end

en_n = max (resampled_energies) - min (resampled_energies) + 1;

%for en = 2 : ne
%	en_n = neutron_energies (en) - neutron_energies (en - 1) - 1
	flux = zeros (nx, ny, nz * 1.3, en_n);

	size (flux)

	for k = 1 : nz
		fprintf (stderr, 'Interpolating slice %d/%d for between energies %.2f MeV and %.2f MeV\n', k, nz, min (resampled_energies), max (resampled_energies));
			
		for i = 1 : nx
			for j = 1 : ny
%				flux (i, j, k, :) = interp1 ([neutron_energies(en - 1) neutron_energies(en)], squeeze (shifted_maps (i, j, k, (en - 1) : en)), (neutron_energies (en - 1) + 1) : (neutron_energies (en) - 1), 'linear', 'extrap');
				flux (i, j, k, :) = interp1 (neutron_energies, squeeze (shifted_maps (i, j, k, :)), min (resampled_energies) : max (resampled_energies), 'pchip', 'extrap');
			end
		end
	end
	
% Shift back & save
	for en_this = 1 : en_n %(neutron_energies (en - 1) + 1) : (neutron_energies (en) - 1)
		fprintf (stderr, 'Saving energy %d/%d (%f MeV)\n', en_this, en_n, resampled_energies (en_this));
		flux (:, :, (1 : nz), en_this) = flux (:, :, interp_offsets (en_this) + (1 : nz), en_this);
%		flux (:, :, (nz - interp_offsets (en_this) + 1) : end, en_this) = 0;
		flattened = squeeze (sum (flux (:, :, :, en_this), 1));
		figure(4); imagesc (flattened); drawnow;
		truncflux = squeeze (flux (:, :, 1 : nz, en_this));
		save (sprintf ('%s/%.0f.mat', output_folder, resampled_energies (en_this)), 'truncflux');
	end
% Save
%end

end
