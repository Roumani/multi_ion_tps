function fluence = calc_neutronfluence (energies, weights)

ne = max (size (energies));
nw = max (size (weights));

if ne ~= nw
	error ('Number of energies should be equal to number of weights.');
end

fluence = 0;

for en = 1 : ne
	tmp = load (sprintf ('neutron_planes/%.0f.mat', energies (en)));
	f = fieldnames (tmp);
	d = getfield (tmp, f{1, 1});

	fluence = fluence + weights (en) * d;
end

% 10x10 because fluence should be in n/cm^2 not /mm^2; 4 because Andrew made an error in his voxel size

fluence = fluence * 4 * 10 * 10;

end
