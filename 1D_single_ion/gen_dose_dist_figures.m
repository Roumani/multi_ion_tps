function gen_dose_dist_figures ()

C = gray (4096);
%C = C (end : -1 : 1, :);

% Load each file; generate figure; voila!

%load ('Proton/xy_100_150.mat');
%load ('Proton/yz_100_150.mat');

xy = load ('Carbon/xy_100_150.mat', '-ascii');
yz = load ('Carbon/yz_100_150.mat', '-ascii');
xy_diff = round ((250 - size (xy, 1)) / 2);
yz_diff = round ((250 - size (yz, 1)) / 2);

%xy = padarray (xy, [xy_diff, xy_diff]);
%yz = padarray (yz, [yz_diff, 0]);

%do_plots (xy, 'proton_xy_100_150');
%do_plots (yz, 'proton_yz_100_150');

%xy = padarray (xy, [xy_diff, xy_diff]);
%yz = padarray (yz, [yz_diff, 0]);

%load ('Proton/xy_140_190.mat');
%load ('Proton/yz_140_190.mat');

%xy = padarray (xy, [xy_diff, xy_diff]);
%yz = padarray (yz, [yz_diff, 0]);

%do_plots (xy, 'proton_xy_140_190');
%do_plots (yz, 'proton_yz_140_190');

%xy = padarray (xy, [xy_diff, xy_diff]);
%yz = padarray (yz, [yz_diff, 0]);

%xy = load ('Carbon/xy_100_150.mat', '-ascii');
%yz = load ('Carbon/yz_100_150.mat', '-ascii');

xy = padarray (xy, [xy_diff, xy_diff]);
yz = padarray (yz, [yz_diff, 0]);

do_plots (xy, 'carbon_xy_100_150');
do_plots (yz, 'carbon_yz_100_150');

xy = padarray (xy, [xy_diff, xy_diff]);
yz = padarray (yz, [yz_diff, 0]);

%load ('Carbon/xy_140_190.mat');
%load ('Carbon/yz_140_190.mat');

%xy = padarray (xy, [xy_diff, xy_diff]);
%yz = padarray (yz, [yz_diff, 0]);

%do_plots (xy, 'carbon_xy_140_190');
%do_plots (yz, 'carbon_yz_140_190');

%xy = padarray (xy, [xy_diff, xy_diff]);
%yz = padarray (yz, [yz_diff, 0]);

function do_plots (data, str);

[nr, nc] = size (data)

x0 = 10;
y0 = 10;
%width = 420;
%height = 400;
%fontsize = 25;

figure(1);
%set (gcf, 'units', 'points', 'position', [x0, y0, width, height]);
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), data);
a = gca;
%set (a, 'FontSize', fontsize);
axis equal;
colormap (C);
cb = colorbar ('eastoutside');
%rectangle ('Position', R, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
drawnow;
print (sprintf ('ddist_yz_centreslice_%s.pdf', str), '-dpdf');

figure(2);
% Normalise
data = data - min (min (min (data)));
data = 100 * data;% / max (max (max (data)));

clf;
hold on;
%set (gcf, 'units', 'points', 'position', [x0, y0, width, height])
%imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), data);
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), medfilt2 (data, [5 5]), 50, 'k');
clabel (c, h, [20 40 60 80 100]);%, 'FontSize', fontsize, 'Color', 'k');
axis equal;
colormap (C);
cb = colorbar ('eastoutside');
%rectangle ('Position', R, 'LineWidth', 3, 'EdgeColor', [1 1 1]);
%a = gca;
%set (a, 'FontSize', fontsize);
drawnow;
hold off;
print (sprintf ('ddist_contour_yz_centreslice_%s.pdf', str), '-dpdf');

end
end
