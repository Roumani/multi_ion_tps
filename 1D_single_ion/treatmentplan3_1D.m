function [final_energy_weights, energies] = treatmentplan3_1D (bio_lib, energies, depth, target_dose)
% Load biological dose profiles for each of the proposed energies

%cmap = gray (256);
%cmap = cmap (end:-1:1, :);
%cmap = jet (256);
%contour_levels = 19;
fontsize = 25;
x0 = 10;
y0 = 10;
imwidth = 3000;
imheight = 1150;
thresh = 0.001;

ne = length (energies);

if ne < 2
	error ('Need at least 2 energies');
end

[nz, ne] = size (bio_lib);

for en = 1 : ne
	dmax = find (bio_lib (:, en) == max (bio_lib (:, en)));
	depths (en) = (round (mean (dmax)) - 1) * 2; % because depth is in 2mm slices here - later we might pass this as one column of the bio library
end

qualified = (depths >= depth (1) & depths <= depth (2));

bio_lib = bio_lib (:, qualified);

energies = energies (qualified);
ne = max (size (energies));

figure(1);
plot ((0 : nz - 1) * 2, bio_lib);
drawnow;

print (sprintf ('unweighted_components_%i_%i.eps', depth (1), depth (2)), '-depsc2');

target = zeros (1, nz);

target ((depth(1) / 2) : (depth(2) / 2)) = target_dose;

size (target)
size (bio_lib)

bio_lib = bio_lib / max (max (bio_lib));

[final_energy_weights, final] = fit_energies (bio_lib', target');

figure (2);
clf;
plot ((0 : nz - 1) * 2, final, '-r', (0 : nz - 1) * 2, target, '-b');
grid on;
xlabel ('Depth (mm)');
ylabel ('Photon-Equivalent Dose (GyE)');
legend ('Biological Dose (GyE)', 'Target Biological Dose (GyE)');
drawnow;
print (sprintf ('target_fit_%i_%i.eps', depth (1), depth (2)), '-depsc2');

energies = energies (find (final_energy_weights > thresh))';
final_energy_weights = final_energy_weights (find (final_energy_weights > thresh));
final_energy_weights = final_energy_weights / sum (final_energy_weights);
