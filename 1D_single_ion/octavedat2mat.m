#!/usr/bin/octave -qHW

if nargin ~= 1
	fprintf (stderr, 'Usage: octavedat2mat filename\n');
	exit (-1);
end

args = argv;

data = load (args{1});

f = fieldnames (data);

data = getfield (data, f{1, 1});

newfname = regexprep (args{1}, '.dat$', '.mat');

save ('-7', newfname, 'data');
