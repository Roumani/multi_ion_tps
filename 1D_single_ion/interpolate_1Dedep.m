% energies is a vector listing the energies in MeV/u
% edep_rbe_files is a cell array of the filenames corresponding to the above energies. Should be the same length!
% resampled_energies is the energies (MeV/u) at which we will interpolate
%
% 1D version of this needed because of Matlab brain damage

function [interp_bio, interp_phy, interp_rbe] = interpolate_1Dedep (energies, edep_rbe_files, resampled_energies)

% We have physical dose and RBE (edep_rbe_files) as a function of depth for multiple energies
stderr = 2;

% This is the number of energies in the original set of data files
ne = max (size (energies));
n_edep_rbe_files = max (size (edep_rbe_files));

if ne ~= n_edep_rbe_files
	error ('Number of supplied energies (MeV/u) should match number of filenames listed in edep_rbe_files');
end

e0 = load (edep_rbe_files{1, end});
depths = e0 (:, 1);
rbe0 = e0 (:, 3);
e0 = e0 (:, 2)'; % last column

first_nz_edep_idx = min (find (e0 > 0));

e0 (1 : first_nz_edep_idx - 1) = e0 (first_nz_edep_idx);

%bio0 = e0 .* interp1 (RBE(:, 1), RBE(:, 2), depths);

ndepths = max (size (e0));

shifted_edeps = zeros (ndepths, max (size (energies)));
shifted_rbes = zeros (ndepths, max (size (energies)));
shifted_edeps (:, end) = e0;
shifted_rbes (:, end) = rbe0;

figure (1); imagesc (e0);

imax = find (e0 == max (max (e0)));

imax_ref = round (mean (imax));

fprintf (stderr, 'Peak index of reference image: %d\n', imax_ref)

offsets = zeros (ne, 1);

for n = 1 : (ne - 1)
	edep = load (edep_rbe_files{1, n});
	rbe = edep (:, 3);
	edep = edep (:, 2)'; % last col

	first_ndepths_edep_idx = min (find (edep > 0));

	edep (1 : first_ndepths_edep_idx - 1) = edep (first_ndepths_edep_idx);
	rbe (1 : first_ndepths_edep_idx - 1) = rbe (first_ndepths_edep_idx);
	
%	bio = edep .* interp1 (RBE(:, 1), RBE(:, 2), depths);
	
	figure (2); imagesc (edep);
	
	imax = find (edep == max (max (edep)));
	imax = round (mean (imax));
	fprintf (stderr, 'Peak index of test image: %d\n', imax)

	offsets (n) = imax_ref - imax;
	
	fprintf (stderr, 'Shifting by %d voxels\n', offsets (n));
	
	shifted_edeps (((1 + offsets (n)) : ndepths), n) = edep (1 : (ndepths - offsets (n)));
	shifted_rbes (((1 + offsets (n)) : ndepths), n) = rbe (1 : (ndepths - offsets (n)));

%	first_ndepths_edep = edep (min (find (edep > 0)))

	shifted_edeps (1 : offsets (n), n) = edep (1);
	shifted_rbes (1 : offsets (n), n) = rbe (1);

	figure (3); subplot (ne, 1, n); imagesc (shifted_edeps (:, n)');
	
	drawnow
end

figure (3); subplot (ne, 1, ne); imagesc (shifted_edeps (:, ne)');

nne = max (size (resampled_energies));

interp_phy = zeros (ndepths, nne);
interp_bio = zeros (ndepths, nne);
interp_rbe = zeros (ndepths, nne);

for k = 1 : ndepths
	fprintf (stderr, 'Interpolating slice %d/%d\n', k, ndepths);
	interp_phy (k, :) = interp1 (energies, squeeze (shifted_edeps (k, :)), resampled_energies, 'linear', 'extrap');
	interp_rbe (k, :) = interp1 (energies, squeeze (shifted_rbes (k, :)), resampled_energies, 'linear', 'extrap');
	interp_bio (k, :) = interp_phy (k, :) .* interp_rbe (k, :);
end

interp_offsets = round (interp1 (energies, offsets, resampled_energies));

for en = 1 : nne
	interp_phy (1 : (ndepths - interp_offsets (en)), en) = interp_phy (1 + interp_offsets (en) : ndepths, en);
	interp_phy ((ndepths - interp_offsets (en) + 1) : end, en) = 0;
	interp_bio (1 : (ndepths - interp_offsets (en)), en) = interp_bio (1 + interp_offsets (en) : ndepths, en);
	interp_bio ((ndepths - interp_offsets (en) + 1) : end, en) = 0;
	interp_rbe (1 : (ndepths - interp_offsets (en)), en) = interp_rbe (1 + interp_offsets (en) : ndepths, en);
	interp_rbe ((ndepths - interp_offsets (en) + 1) : end, en) = 0;
%	figure(4); imagesc(interp_rbe (:, en)); drawnow;
end

end
