% edep_energies is a vector listing the energies in MeV/u
% edep_files is a cell array of the filenames corresponding to the above energies. Should be the same length!
% resampled_energies is the energies (MeV/u) at which we will interpolate

function interps = interpolate_edep (edep_energies, edep_files, resampled_energies)

stderr = 2;

ne = max (size (edep_energies));
n_edep_files = max (size (edep_files));

if ne ~= n_edep_files
	error ('Number of supplied energies (MeV/u) should match number of filenames listed in edep_files');
end

e0 = csvread (edep_files{1, end}, 1);
e03d = list23dvol (e0);
[nx, ny, nz] = size (e03d);

shifted_maps = zeros (nx, ny, nz, max (size (edep_energies)));

shifted_maps (:, :, :, end) = e03d;

flattened = squeeze (sum (e03d, 1));

figure (1); imagesc (flattened);

[rmax, cmax] = find (flattened == max (max (flattened)));

cmax_ref = round (mean (cmax));

fprintf (stderr, 'Peak index of reference image: %d\n', cmax_ref)

offsets = zeros (ne, 1);

for n = 1 : (ne - 1)
	edep = csvread (edep_files{1, n}, 1);
	edep3d = list23dvol (edep);
	
	flattened = squeeze (sum (edep3d, 1));
	
	figure (2); imagesc (flattened);
	
	[rmax, cmax] = find (flattened == max (max (flattened)));
	cmax = round (mean (cmax));
	fprintf (stderr, 'Peak index of test image: %d\n', cmax)

	offsets (n) = cmax_ref - cmax;
	
	fprintf (stderr, 'Shifting by %d voxels\n', offsets (n));
	
	shifted_maps (:, :, (offsets (n) : nz), n) = edep3d (:, :, 1 : (nz - offsets (n) + 1));

	for k = 1 : (offsets (n) - 1)
		shifted_maps (:, :, k, n) = edep3d (:, :, 1);
	end

	figure (3); subplot (ne, 1, n); imagesc (squeeze (sum (shifted_maps (:, :, :, n), 1)));
	
	drawnow
end

figure (3); subplot (ne, 1, ne); imagesc (squeeze (sum (shifted_maps (:, :, :, ne), 1)));

nne = max (size (resampled_energies));

interps = zeros (nx, ny, nz, nne);

for k = 1 : nz
	fprintf (stderr, 'Interpolating slice %d/%d\n', k, nz);
	
	for i = 1 : nx
		for j = 1 : ny
			interps (i, j, k, :) = interp1 (edep_energies, squeeze (shifted_maps (i, j, k, :)), resampled_energies, 'linear', 'extrap');
		end
	end
end

interp_offsets = round (interp1 (edep_energies, offsets, resampled_energies));

for en = 1 : nne
	interps (:, :, 1 : (nz - interp_offsets (en)), en) = interps (:, :, 1 + interp_offsets (en) : nz, en);
	interps (:, :, (nz - interp_offsets (en) + 1) : end, en) = 0;
	flattened = squeeze (sum (interps (:, :, :, en), 1));
	figure(4); imagesc(flattened); drawnow;
end

end
