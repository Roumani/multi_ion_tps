Multi_ion =  [struct('ion', 'H', 'en', [79 95 110 131 151 162 172 184 196], 'X', [0:100], 'Y', [0:100], 'Z', [0:300]);...
struct('ion', 'He', 'en', [79 95 110 131 151 162 172 184 196], 'X', [0:100], 'Y', [0:100], 'Z', [0:300]);...
struct('ion', 'Li', 'en', [90 109 126 152 175 187 199 213 277], 'X', [0:100], 'Y', [0:100], 'Z', [0:300]);...
struct('ion', 'C', 'en', [146 177 205 290 309 329 254 378],'X', [0:100], 'Y', [0:100], 'Z', [0:300]);...
struct('ion', 'Fe', 'en', [341 407 477 590 666 753 820 880 948], 'X', [0:100], 'Y', [0:100], 'Z', [0:300])]




ion_names = {'H', 'Li', 'He', 'C', 'Fe'};

% Hard code for now; we can read this from the first ddep file for the first ion
xdim = 100;
ydim = 100;
zdim = 300;

% The assumption here is that the folders are structured like this:

% H/100.mat
% H/120.mat
% C/150.mat

% and so on. Basically ion/energy.mat

for n = 1 : length (ion_names)
ion_data{n}.name = ion_names{n};

% Obtain energies for this particular ion - get a list of all the files in the folder for this ion
energies_list =  dir(strcat (ion_names{n},'/*.mat'))
%energies_list = dir(ion_names{n});
energies_list = energies_list(3:end);

ion_data{n}.energies = energies_list;
n_energies = max (size (ion_data{n}.energies));

ion_data{n}.ddep = zeros (n_energies, xdim, ydim, zdim);

for en_idx = 1 : n_energies
ion_data{n}.ddep (en_idx, :, :, :) = load (sprintf ('%s/%s', ion_data{n}.name, ion_data{n}.energies(en_idx).name))
    %ion_data{n}.ddep (en_idx, :, :, :) = load (sprintf ('%s/%i.mat', ion_data{n}.name, ion_data{n}.energies))
end
end