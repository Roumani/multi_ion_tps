% Provide a list of energies

function [final_energy_weights, weighted_dose, energies] = treatmentplan2 (energies, energy_lib_path, physbio, depth, width, height, target_dose)

if nargin ~= 7
	error ('Usage: [final_energy_weights, weighted_dose, energies] = treatmentplan2 (energies, energy_lib_path, physbio, depth, width, height, target_dose)');
end

if strcmp (physbio, 'physical')
	units = 'Gy';
elseif strcmp (physbio, 'biological')
	units = 'Gy(RBE)';
else
	error ('Third argument should either be physical or biological');
end

% Load dose profiles for each of the proposed energies

%cmap = gray (256);
%cmap = cmap (end:-1:1, :);
cmap = jet (256);
contour_levels = 19;
lw = 2;
fontsize = 25;
x0 = 10;
y0 = 10;
imwidth = 3000;
imheight = 1150;

ne = length (energies);

if ne < 2
	error ('Need at least 2 energies');
end

tmp = load (sprintf ('%s/%.0f.mat', energy_lib_path, energies (1)));

f = fieldnames (tmp);

d = getfield (tmp, f{1, 1});

[nx, ny, nz] = size (d);

components = zeros (nx, ny, nz, ne);

%flat_components = zeros (nz, ne);

components (:, :, :, 1) = d;

depths = zeros (ne, 1);
flattened = squeeze (sum (d));
[~, cmax] = find (flattened == max (max (flattened)));
depths (1) = round (mean (cmax));

for en = 2 : ne
	tmp = load (sprintf ('%s/%.0f.mat', energy_lib_path, energies (en)));

	f = fieldnames (tmp);

	d = getfield (tmp, f{1, 1});

	flattened = squeeze (sum (d));
	[~, cmax] = find (flattened == max (max (flattened)));
	depths (en) = round (mean (cmax));

	components (:, :, :, en) = d;
end

figure(1);

plot (energies, depths);

qualified = (depths >= depth (1) & depths <= depth (2));

components = components (:, :, :, qualified);
energies = energies (qualified);
ne = max (size (energies));

tpv = components (floor (nx / 2) + (width(1) : width(2)), floor (ny / 2) + (height(1) : height(2)), :, :);

flat_components = squeeze (mean (mean (tpv)));

figure(2);
plot (flat_components);
drawnow;

print (sprintf ('unweighted_components_%i_%i.eps', depth (1), depth (2)), '-depsc2');

target = zeros (1, nz);

target (depth(1) : depth(2)) = target_dose;

[final_energy_weights, final] = fit_energies (flat_components', target);

%size (target)
%size (final)

figure (3);
plot (0 : nz - 1, final, 0 : nz - 1, target);
grid on;
xlabel ('Depth (mm)');
ylabel (units);
legend ('Fit', 'Target');
drawnow;
print (sprintf ('target_fit_%i_%i.eps', depth (1), depth (2)), '-depsc2');

weighted_dose = zeros (nx, ny, nz);

for en = 1 : ne
	weighted_dose = weighted_dose + components (:, :, :, en) * final_energy_weights (en);
end

save (sprintf ('weighted_dose_%i_%i.mat', depth(1), depth(2)), 'weighted_dose');

flattened = squeeze (mean (weighted_dose ((-25 : 25) + ceil (nx / 2), :, :), 1));

save (sprintf ('flattened_%i_%i.mat', depth(1), depth(2)), 'flattened');

yz = squeeze (weighted_dose (round (nx / 2), :, :));

save (sprintf ('yz_%i_%i.mat', depth(1), depth(2)), 'yz');

xy = squeeze (weighted_dose (:, :, round (nz / 2)));

save (sprintf ('xy_%i_%i.mat', depth(1), depth(2)), 'xy');

%return;

s = yz;

[nr, nc] = size (flattened);

figure (4);
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), flattened);
axis equal;
colormap (cmap);
cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_yz_flattened_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

imwrite (flattened / max (max (flattened)) * 256, cmap, sprintf ('dosedist_yz_flattened_%i_%i.png', round (depth (1)), round (depth (2))));

figure(5);

set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s);
axis equal;
colormap (cmap);
cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_yz_centreslice_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

imwrite (s / max (max (s)) * 256, cmap, sprintf ('dosedist_yz_centreslice_%i_%i.png', round (depth (1)), round (depth (2))));

s2 = round (s * 99);
f2 = round (flattened * 99);
s2f = medfilt2 (s2, [5, 5]);
f2f = medfilt2 (f2, [5, 5]);
 
figure(6);
clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
%imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), f2);
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), f2f, contour_levels, 'LineWidth', lw);
clabel (c, h, 'FontSize', fontsize, 'Color', 'r');
axis equal;
colormap (cmap);
%cb = colorbar ('eastoutside');
drawnow;
hold off;
print (sprintf ('dosedist_contour_yz_flattened_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

figure(7);
clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
%imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s2);
s_max = max (max (s2))
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s2f, contour_levels, 'LineWidth', lw);
clabel (c, h, 'FontSize', fontsize, 'Color', 'r');
axis equal;
colormap (cmap);
%cb = colorbar ('eastoutside');
drawnow;
hold off;
print (sprintf ('dosedist_contour_yz_centreslice_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

flattened (flattened > target_dose) = target_dose;
s (s > target_dose) = target_dose;

f2 = round (flattened * 99);
s2 = round (s * 99);

figure (8);
clf;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), flattened);
axis equal;
colormap (cmap);
cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_yz_flattened_clamped_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

imwrite (flattened / max (max (flattened)) * 256, cmap, sprintf ('dosedist_yz_flattened_clamped_%i_%i.png', round (depth (1)), round (depth (2))));

figure(9);
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s);
axis equal;
colormap (cmap);
cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_yz_centreslice_clamped_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

imwrite (s / max (max (s)) * 256, cmap, sprintf ('dosedist_yz_centreslice_clamped_%i_%i.png', round (depth (1)), round (depth (2))));

figure(10);
clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
%imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s2);
s_max = max (max (s2))
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), s2f, contour_levels, 'LineWidth', lw);
clabel (c, h, 'FontSize', fontsize, 'Color', 'r');
axis equal;
colormap (cmap);
%cb = colorbar ('eastoutside');
drawnow;
hold off;
print (sprintf ('dosedist_contour_yz_centreslice_clamped_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');

figure(11);
clf;
hold on;
set (gcf, 'units', 'points', 'position', [x0, y0, imwidth, imheight])
%imagesc (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), f2);
[c, h] = contour (0 : nc - 1, (0 : nr - 1) - floor (nr / 2), f2f, contour_levels, 'LineWidth', lw);
axis equal;
colormap (cmap);
%cb = colorbar ('eastoutside');
drawnow;
print (sprintf ('dosedist_contour_yz_flattened_clamped_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');
clabel (c, h, 'FontSize', fontsize, 'Color', 'r', 'labelspacing', 300);
print (sprintf ('dosedist_contour_yz_flattened_clamped_labelled_%i_%i.eps', round (depth (1)), round (depth (2))), '-depsc2');
hold off;

end
