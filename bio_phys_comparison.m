function bio_phys_comparison(bio,phys)

% Usage: prior to the usage of the function, upload data as:  
% phys = load('ion_data_small_six_ions.mat');
% bio = load ('ion_data_bio_dose.mat');
% then bio_phys_comparison(bio,phys)

 tmp = [];
    for k = 1 : length (phys.ion_data)
    
% if strcmp (phys.ion_data{k}.name, ion)
% ion_idx = k;
% break;
% end
    
         for    en = [1,50,100,110, length(bio.ion_data{k}.energies)] %MeV/u
                % energy = phys.ion_data{1}.energies( phys.ion_data{1}.maxdosedepth == depth); 
                %en = find (bio.ion_data{1}.energies == energy);
                energy = phys.ion_data{k}.energies(en); 
                 % en = find (bio.ion_data{k}.energies == energy);


                tmp = [tmp energy]

                tmp_phys = squeeze(phys.ion_data{k}.ddep(en,:,:,:));
                tmp_bio =  squeeze(bio.ion_data{k}.ddep(en,:,:,:));



                A = squeeze (sum (tmp_phys, 3));

                xproj_phys = sum (A, 2);
                %yproj_phys = sum (A, 1);

                xmaxidx_phys = (xproj_phys == max (xproj_phys));
                %ymaxidx_phys = (yproj_phys == max (yproj_phys));


                B = squeeze (sum (tmp_bio, 3));

                xproj_bio = sum (B, 2);
                %yproj_bio = sum (B, 1);

                xmaxidx_bio = (xproj_bio == max (xproj_bio));
                %ymaxidx_bio = (yproj_bio == max (yproj_bio));

                % Set coloarbar scale for the first figure 
                top = max(max(max(squeeze (tmp_phys (xmaxidx_phys, :, :)))), max(max(squeeze (tmp_bio (xmaxidx_bio, :, :)))));  
                bottom =min(min(min(squeeze (tmp_phys (xmaxidx_phys, :, :)))), min(min(squeeze (tmp_bio (xmaxidx_bio, :, :))))); 

                figure (1);
                subplot (2, 1,1);
                imagesc (squeeze (tmp_phys (xmaxidx_phys, :, :)));
                xlabel ('Z (depth) (mm)');
                ylabel ('Y (vertical) (mm)');
                axis equal;
                colorbar;
                %grid on;
                title (['YZ-slice, physical dose, '  phys.ion_data{k}.name ' ' sprintf('%.2f', energy) ' MeV/u (Gy(RBE)/primary particle)']);
                caxis manual;
                caxis([bottom top])

                subplot(2,1,2)
                imagesc (squeeze (tmp_bio (xmaxidx_bio, :, :)));
                xlabel ('Z (depth) (mm)');
                ylabel ('Y (vertical) (mm)');
                axis equal;
                colorbar;
                %grid on;
                title (['YZ-slice, biological dose, '  phys.ion_data{k}.name ' ' sprintf('%.2f', energy) ' MeV/u (Gy(RBE)/primary particle)']);
                caxis manual;
                caxis([bottom top]);
                P2 =  ['YZ_' phys.ion_data{k}.name '_' sprintf('%.2f', energy)];
                print([P2 '.eps'], '-depsc2');
                system (['epstopdf ' P2 '.eps']);
                system (['pdftrimwhite ' P2 '.pdf']);

                %print ('YZ_Li159.eps', '-depsc2');
                %system ('epstopdf YZ_Li159.eps && pdftrimwhite YZ_Li159.pdf');

                % Side view
                C = squeeze (sum (tmp_phys, 1));

                zproj_phys = sum (C, 1);

                zmaxidx_phys = (zproj_phys == max (zproj_phys));

                D = squeeze (sum (tmp_bio, 1));

                zproj_bio = sum (D, 1);

                zmaxidx_bio = (zproj_bio == max (zproj_bio));

                %Set colorbar scale for the second figure 
                top_1 = max(max(max(squeeze (tmp_phys (:, :, zmaxidx_phys)))), max(max(squeeze (tmp_bio (:, :, zmaxidx_bio)))));  
                bottom_1 =  min(min(min(squeeze (tmp_phys (:, :, zmaxidx_phys)))), min(min(squeeze (tmp_bio (:, :, zmaxidx_bio))))); 


                figure(2)

                subplot(2,1,1)
                imagesc (squeeze (tmp_phys (:, :, zmaxidx_phys)));
                xlabel ('Z (depth) (mm)');
                ylabel ('Y (vertical) (mm)')
                axis equal;
                colorbar;
                %grid on;
                caxis manual;
                caxis([bottom_1 top_1]);
                title (['XY-slice, physical dose, '  phys.ion_data{k}.name ' ' sprintf('%.2f', energy) ' MeV/u (Gy(RBE)/primary particle)']);

                subplot(2,1,2)
                imagesc (squeeze (tmp_bio (:, :, zmaxidx_bio)));
                xlabel ('Z (depth) (mm)');
                ylabel ('Y (vertical) (mm)');
                axis equal;
                colorbar;
                %grid on;
                caxis manual;
                caxis([bottom_1 top_1]); 
                title (['XY-slice, biological dose, '  phys.ion_data{k}.name ' ' sprintf('%.2f', energy) ' MeV/u (Gy(RBE)/primary particle)']);
                P1 =  ['XY_' phys.ion_data{k}.name '_' sprintf('%.2f', energy)];
                print([P1 '.eps'], '-depsc2');
                system (['epstopdf ' P1 '.eps']);
                system (['pdftrimwhite ' P1 '.pdf']);
                %print ('XY_Li159.eps', '-depsc2');
                %system ('epstopdf XY_Li159.eps && pdftrimwhite XY_Li159.pdf');
                %print ([P '.pdf'], '-dpdf');


         end
end
