function [T_by_name, T_by_value] = get_labels ()

% H is the peak scaling factor
% C is the range scaling factor
% ** At the moment we are assuming this is all the same for all ions, will check this via simulations!

 % As reported in literature; WER = ~1.157 for protons, helium, carbon and iron e.g. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2977971/

%T = [
%struct('Name', 'Air', 'PER', 0);
%struct('Name', 'Water', 'PER', 1 / 1.157);
%struct('Name', 'Skin', 'PER', 1 / 1.157);
%struct('Name', 'Bone', 'PER', 1 / 1.157);
%struct('Name', 'Brain', 'PER', 1 / 1.157);
%struct('Name', 'Tumour', 'PER', 1 / 1.157);
%];

%Thash = containers.Map({T.Name}, T, 'ValueType', 'struct');

T_by_name = struct();

% PERR = PMMA Equivalent Range Ratio
% PEDR = PMMA Equivalent Dose Ratio

% THESE NEED TO ALL BE CHANGED!

% Note: we start the values at 1 rather than zero, because... you know, Matlab :-)

%PMMA_WER = 1.157;

% Water equivalent thickness is the thickness of water which will result in the same depth
% Water equivalent ratio is the ratio of thickness in water to thickness in material
% So for PMMA, WER = 1.157 meaning that depth in water is 1.157x depth in PMMA for same energy.
% Hence the PMMA-equivalent ratio for water is 1 / 1.157.

T_by_name.('Air') = struct ('value', 1, 'PERR', 0, 'PEDR', 1, 'density', 1); % 1.25e-3
T_by_name.('Water') = struct ('value', 2, 'PERR', 0.866, 'PEDR', 1, 'density', 1.0);
T_by_name.('Skin') = struct ('value', 3, 'PERR', 0.9361666667, 'PEDR', 1, 'density', 1.09);
T_by_name.('Bone') = struct ('value', 4, 'PERR', 1.478833333, 'PEDR', 1, 'density', 1.92); %[1]
T_by_name.('Brain') = struct ('value', 5, 'PERR', 0.8985998556, 'PEDR', 1, 'density', 1.04); 
T_by_name.('Adipose') = struct ('value', 6, 'PERR', 0.84435, 'PEDR', 1, 'density', 0.94); %[2]
T_by_name.('Tumour') = struct ('value', 7, 'PERR', 0.8985998556, 'PEDR', 1, 'density', 1.04);
T_by_name.('PMMA') = struct ('value', 8, 'PERR', 1, 'PEDR', 1, 'density', 1.19);

% [1] https://www.researchgate.net/publication/326167999_Calculation_of_water_equivalent_ratios_WER_for_various_materials_at_proton_energies_ranging_10-500_MeV_using_MCNP_FLUKA_and_GEANT4_Monte_Carlo_codes

% Generate by-value version of T_by_name
fn = fieldnames (T_by_name);

for k = 1 : numel (fn)
	T_by_name.(fn{k}).Name = fn{k};
	T_by_value (T_by_name.(fn{k}).('value')) = T_by_name.(fn{k});
end
