function energies_MeV = ion_energies (depths_cm)
stderr = 2;


ions_list = {'H', 'He', 'Li', 'C', 'O', 'Ne', 'Si', 'Fe'};
atomic_num = [1 2 3 6 8 10 14 26];
mass_num =  [1 4 7 12 16 20 28 56];


rho_pmma = 1.19000; % g/cm^3

%p = dlmread ('proton_depth.dat', " ", 7, 0);
p = readmatrix('proton_depth.dat');

[en_idx, idx] = unique (p (:, 1));

en_idx = p (:, 1);
depths = p (:, 2) / rho_pmma;

en_idx = en_idx (idx);
depths = depths (idx);


for ion = 1 : numel(ions_list)

	ions_library{ion}.name = ions_list{ion};

	ions_library{ion}.Z = atomic_num(ion);

	ions_library{ion}.A = mass_num(ion);

	ions_library{ion}.depths_cm =  ((ions_library{ion}.Z / ions_library{1}.Z) ^2) * (ions_library{1}.A / ions_library{ion}.A)* depths_cm;
	
	ions_library{ion}.energies_MeV = interp1 (depths, en_idx, ions_library{ion}.depths_cm);
end

%depths_cm =  ((ions_library{ion}.Z / ions_library{1}.Z) ^2) * (ions_library{1}.M / ions_library{ion}.M)* depths_cm;
%energies_MeV = interp1 (depths, en_idx, depths_cm);


 for ion = 1 : length(ions_list)
	for en_idx = 1 : length(ions_library{1}.energies_MeV)


		F = [ions_list{ion}, '/'] + sprintf("%.1f",ions_library{ion}.energies_MeV(en_idx));
		%F = [ions_list{ion}, '/'] + sprintf("%d_MeV",round(energies_MeV(en_idx)));


		if ~exist (F, 'dir')
			mkdir(F);
		end

		energy_file = fopen (F + '/en.mac', 'w');
		fprintf(energy_file, '# energy distribution\n');
		fprintf(energy_file,'/gps/ene/type Gauss\n');
		fprintf(energy_file,'/control/alias energyPerNucleon %.1f\n', ions_library{ion}.energies_MeV(en_idx));
		fprintf(energy_file,'/control/multiply totalEnergy {energyPerNucleon} %i\n',ions_library{ion}.A);
		fprintf(energy_file,'/control/multiply spreadEnergy {totalEnergy} 0.002\n');
		fprintf(energy_file,'/gps/ene/mono {totalEnergy} MeV\n');
		fprintf(energy_file,'/gps/ene/sigma {spreadEnergy} MeV\n');

		fclose(energy_file);
% Create a symoblic link for ion.mac and master run.mac
		system ("ln -sf  ../ion.mac ../../run.mac " + F);
	end

end 
