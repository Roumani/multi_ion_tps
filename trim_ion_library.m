function ion_data = trim_ion_library (ion_data_full, halfwidth)

[ne, nx, ny, nz] = size (ion_data_full{1}.ddep);

for n = 1 : length (ion_data_full)
	ion_data{n}.name = ion_data_full{n}.name;

	ion_data{n}.energies = ion_data_full{n}.energies;
	ion_data{n}.A = ion_data_full{n}.A;
	ion_data{n}.Z = ion_data_full{n}.Z;

% Create space for dose deposition data_small
	ion_data{n}.ddep = ion_data_full{n}.ddep (:, round (nx / 2) + (-halfwidth : halfwidth), round (ny / 2) + (-halfwidth : halfwidth), :);
	ion_data{n}.maxdosedepth = ion_data_full{n}.maxdosedepth;
end





%usage:

%ion_data = trim_ion_library (ion_data_full, 10);
%the 10 means that it will take the centre -10:10
%so 21 samples including the central one
save -v7.3 simulated_ion_data_small_set.mat ion_data; 
