function [phantom, target] = create_rectangle_prism_phantom_target (xpix, ypix, zpix, centre_x, centre_y, centre_z, dose, debug)

[TBN, TBV] = get_labels ();

%Create a rectangle prism target

target = zeros (xpix, ypix, zpix);
target (centre_x, centre_y, centre_z) = 1;
target = target * dose;

phantom = ones (xpix, ypix, zpix);
phantom = phantom * TBN.PMMA.value;

%minph = min (phantom(:))
%maxp=max (phantom(:))

%mint= min(target(:))
%maxt= max(target(:))

%min (spherical_target(:))
%max (spherical_target(:))

% Multiply the spherical region of voxels by a value of a desired dose 

if nargin == 8 && debug
	figure(1)
	imagesc(squeeze(target(round (size (target, 1) / 2), :, :)))
	axis equal;
	colorbar;
	figure(2)
	imagesc(squeeze(target(:, round (size (target, 2) / 2), :)))
	axis equal;
	colorbar;
	figure(3)
	imagesc(squeeze(target(:, :, round (size (target, 3) / 2))))
	axis equal;
	colorbar;

	figure(4)
	imagesc(squeeze(phantom(round (size (phantom, 1) / 2), :, :)))
	axis equal;
	colorbar;
	figure(5)
	imagesc(squeeze(phantom(:, round (size (phantom, 2) / 2), :)))
	axis equal;
	colorbar;
	figure(6)
	imagesc(squeeze(phantom(:, :, round (size (phantom, 3) / 2))))
	axis equal;
	colorbar;
end
