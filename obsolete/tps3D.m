% target_volume should be a 3D voxelised volume with nonzero values for wherever we want some dose deposited.
%
% library should be a cell array of ion structures, which have the following elements:
%
% .name (e.g. 'H', 'Li' etc.)
% .energies (in MeV/u, e.g. [150 175 ... ])
% .ddata (4D volumetric dose distribution array; [energy_idx, x, y, z])
% .maxdepthdose (voxel depth of peak)
%
% decimations should be a vector of four numbers used to weight the cost function: [target behind around infront]
%
% these numbers do not need to be normalised.
%
% grid_x and grid_y are the steps (in voxels) between the raster scan steps in x and y, respectively
%
% latticetype is an optional argument; it can be either 'rect' or 'tri'. If
% 'tri' then grid_y is ignored and the beamgrid is an equilateral triangular
% lattice.
%
% TODO: think about scaling to mm for various things (?)

function [plan, C, d, x, resnorm, residual] = tps3D_triangular (target_volume, library, decimations, grid_x, grid_y, latticetype)

% TODO: sanity check - make sure target_volume dimensions are same as library ddist dimensions
% This may not be critical but we need to think about it. Don't do it yet.

% First step: segment the volume into four subsets:

% 1. The target itself
% 2. In front of the target
% 3. Behind the target
% 4. Around the target
%
% The objective of the TPS is to achieve the planned dose within the target,
% with minimum dose behind and around the target (which can feasibly be
% near-zero) and minimum dose in front of the target (although this is much
% less important than reaching the planned dose within the target, and it is
% understood that this cannot be zero).

% Work out the dimensions of the target

[targ_x, targ_y, targ_z] = size (target_volume);

% Flatten Z axis to create XY projection - determine range of X, Y where we need to apply some dose

xy_plane = squeeze (sum (target_volume, 3));

% These are the full list of x and y indices in the raster-scan mesh; these get re-used quite a bit

stderr = 2;
fprintf (stderr, 'Lattice type is %s\n', latticetype);

if nargin < 6 || (nargin == 6 && strcmp (latticetype, 'rect'))
	raster_x = 1 : grid_x : targ_x;
	raster_y = 1 : grid_y : targ_y;
elseif strcmp (latticetype, 'tri')
	raster_x = round (1 : (grid_x / 2) : targ_x);
	raster_y = round (1 : (grid_x * sqrt(3) / 2) : targ_y);
else
	error ('Unknown lattice type (should be rect or tri)');
end

% Generate subset of full resolution XY plane where we have some dose (0 or 1; 1 = member of set)
targ_xy_reg = (xy_plane > 0);
% Generate subset of full resolution XY plane where we want zero dose (0 or 1; 1 = member of set)
around_xy_reg = (xy_plane == 0);

% Of the points in our mesh, which are in each subset? This is a truth value (0 or 1)
targ_mask = targ_xy_reg (raster_x, raster_y);
%around_mask = around_xy_reg (raster_x, raster_y);

% What are the index pairs of these?

% First the target - these are all the X, Y coordinates where we will be
% aiming the beam (with different ions/energies in each case). The Z
% placeholders are for the min & max depth for each XY pair, which we will calculate next.

[xi, yi] = find (targ_mask);
zi = zeros (size(xi));
targ_beam_coord_list = [raster_x(xi)', raster_y(yi)', zi, zi];

if strcmp (latticetype, 'tri')
	xodd = raster_x (1:2:end);
	xeven = raster_x (2:2:end);
	yodd = raster_y (1:2:end);
	yeven = raster_y (2:2:end);

	%clf;
	%hold on;
	%plot (targ_beam_coord_list(:, 1) + i * targ_beam_coord_list(:, 2), 'xr');

	targ_beam_coord_list ((sum ((targ_beam_coord_list (:, 1) == xeven), 2) & sum ((targ_beam_coord_list (:, 2) == yodd), 2)) | (sum ((targ_beam_coord_list (:, 1) == xodd), 2) & sum ((targ_beam_coord_list (:, 2) == yeven), 2)), :) = [];

	%plot (targ_beam_coord_list(:, 1) + i * targ_beam_coord_list(:, 2), '*b');
	%axis equal;
	%hold off;
	%pause;
end

% And for the full-resolution mask - this will be used for segmenting the
% volume for the evaluation of the quality of the treatment plan.

% These are the ones where we don't need to direct any beams. HOWEVER, we
% want to make sure the dose going here is as low as possible. No need for
% any Z here.

%[xi, yi] = find (around_xy_reg);
%around_full_coord_list = [xi, yi];

% OK, now what are the depth ranges for our target for each of the X, Y?
% This is how we will determine the subset of ions/energies that can
% potentially be used for each X, Y point.

% How many raster points?
n_raster_points = size (targ_beam_coord_list, 1);

% Go through all of them and find the depth range
% Later: add the corresponding energy range for each ion species that we can use for each point (?)
for beampos = 1 : n_raster_points
	nz = find (target_volume (targ_beam_coord_list (beampos, 1), targ_beam_coord_list (beampos, 2), :));
	targ_beam_coord_list (beampos, 3) = min (nz);
	targ_beam_coord_list (beampos, 4) = max (nz);
end

% Cell array of vectors of weights for each energy for each ion species
for ion = 1 : length (library)
	energy_set{ion} = zeros (length (library{ion}.energies), 1);
%	plan.ions{ion} = library{ion}.name;
end

plan.beamgrid = targ_beam_coord_list;

% Initialise to zeros, with some random positive value in those coefficients
% where we might actually have some ability to deposit dose
for beampos = 1 : length (targ_beam_coord_list)
% Weights
	plan.weights{beampos} = energy_set;
% Permitted energy mask for this ion & position
	plan.emask{beampos} = energy_set;

	for ion = 1 : length (library)
		i0 = find (library{ion}.maxdosedepth > targ_beam_coord_list (beampos, 3)) - 1;
		i1 = find (library{ion}.maxdosedepth > targ_beam_coord_list (beampos, 4)) - 1;
		plan.emask{beampos}{ion}(i0:i1) = 1;
%		plan.weights{beampos}{ion}(i0:i1) = 1e7 * (i0:i1).^3;
	end
end

plan.target_volume = target_volume;
plan.targ_x = targ_x;
plan.targ_y = targ_y;
plan.targ_z = targ_z;

% We now have an empty "plan" - the beam positions, the weights/masks for
% each energy for each ion and the target dimensions.

% Copy weights from plan.weights into k vector (yuck, but have to do this
% for now - maybe do this more efficiently later)

% This is super inefficient but actually in this case it is acceptable as
% the cost is one-off

%n = 1;
%
%for beampos = 1 : length (plan.beamgrid)
%	for ion = 1 : length (plan.weights{beampos})
%		for w = 1 : length (plan.weights{beampos}{ion})
%			if plan.emask{beampos}{ion}(w)
%				D0 (n) = plan.weights{beampos}{ion}(w);
%				n = n + 1; 
%			end
%%		end
%	end
%end

%options = optimoptions ('lsqnonneg', 'MaxIter', 1000, 'Display', 'iter');

[C, d] = generate_dose_matrix (plan, target_volume, decimations, library);

%size (C)

% Find x such that ||Cx - d||^2 is minimised

fprintf (stderr, 'Starting non-negative least squares optimisation...\n');

[x, resnorm, residual] = lsqnonneg (C, d);

fprintf (stderr, 'Done\n');

% Now for ease of reading we transfer the solution back into the treatment plan struture
n = 1;

for beampos = 1 : length (plan.beamgrid)
	for ion = 1 : length (plan.weights{beampos})
		for w = 1 : length (plan.weights{beampos}{ion})
			if plan.emask{beampos}{ion}(w)
				plan.weights{beampos}{ion}(w) = x(n);
				n = n + 1;
			end
		end
	end
end
