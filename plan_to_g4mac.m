function plan_to_g4mac (plan, library, reduction_factor)

if nargin <= 2
	reduction_factor = 1;
end

% Define W as the width of the energy peak (MeV)
W = 1;

% Define epsilon as the width of the rising or falling edge
epsilon = 0.01;

%ions_list= {'H', 'He', 'Li', 'C', 'O', 'Ne'};
%ions_list = library

[xdim, ydim, zdim] = size (plan.target_volume);
half_x = xdim / 2;
half_y = ydim / 2;

for ion = 1 : length(library)
	for pos_idx = 1 : size (plan.beamgrid, 1)
		x_coord = plan.beamgrid (pos_idx, 1);
		y_coord = plan.beamgrid (pos_idx, 2);
	  %  F = fullfile(strcat (library{ion}.name), sprintf("%d_%d" ,x_coord,y_coord));

		F = [library{ion}.name, '/'] + sprintf("%d_%d", x_coord, y_coord); 

% Only create the folder if it doesn't exist
		if ~exist (F, 'dir')
			mkdir(F);
		end
		
		x_coord = x_coord - half_x;
		y_coord = y_coord - half_y;
			
		position_file = fopen (F + '/position.mac', 'w');
		fprintf (position_file, '/gps/pos/centre %.0f %.0f -150 mm\n', x_coord, y_coord);
		fclose (position_file);

		en = plan.energies{pos_idx}{ion};
		weight = plan.weights{pos_idx}{ion};
		
		en = en(weight ~= 0);
		weight = weight (weight ~= 0);
		
		en_trap = zeros (length (en) * 4, 1);
		weight_trap = zeros (length (en) * 4, 1);
		
		n_primaries = 0;
		
		for i = 1 : length(en)
			en_trap((i - 1) * 4 + 1) = (en (i) - W / 2 - epsilon / 2) * library{ion}.Z;
			en_trap((i - 1) * 4 + 2) = (en (i) - W / 2 + epsilon / 2) * library{ion}.Z;
			en_trap((i - 1) * 4 + 3) = (en (i) + W / 2 - epsilon / 2) * library{ion}.Z;
			en_trap((i - 1) * 4 + 4) = (en (i) + W / 2 + epsilon / 2) * library{ion}.Z;

			weight_trap (i * 4 - 3) = 0;
			weight_trap (i * 4 - 2) = weight (i);
			weight_trap (i * 4 - 1) = weight (i);
			weight_trap (i * 4) = 0;

			n_primaries = n_primaries + weight(i);
		end

		weight_trap = weight_trap / (W * n_primaries);

		spectrum_file = fopen (F + '/spectrum.mac', 'w');
		fprintf(spectrum_file, '# energy distribution\n');
		fprintf(spectrum_file,'/gps/ene/type Epn\n');
		fprintf(spectrum_file,'/gps/hist/type epn\n');

		fprintf(spectrum_file,'/gps/hist/point 0.0  0.0\n');
		
		for k = 1: length(en_trap)
			fprintf(spectrum_file,'/gps/hist/point %.3f  %.6f\n', en_trap(k), weight_trap(k));
		end

		if length(en)
			fprintf(spectrum_file,'/gps/hist/point %.3f  0.0\n', en_trap(k) * 100);
		end

		fclose(spectrum_file);  

		number_of_primaries_file = fopen (F + '/primaries.mac', 'w');
		fprintf(number_of_primaries_file, '# number of primaries\n');
		fprintf(number_of_primaries_file,'/control/alias numberPrimaries %d\n', round (n_primaries / reduction_factor));		   

		fclose(number_of_primaries_file);
% Create a symoblic link for ion.mac and master run.mac

		system ("ln -sf ../ion.mac ../../run.mac " + F);
	end  
end 
end


