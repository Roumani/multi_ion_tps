% Load ion_data if we haven't already done so - this saves a lot of time!

stderr = 2;

if ~exist ('ion_data', 'var')
	fprintf (stderr, 'Loading ion data from current working directory...\n'); 
%	ion_data = load_ion_library;
	load simulated_ion_data_small_set.mat
	fprintf (stderr, 'Loading complete.\n')
else
	fprintf (stderr, 'Ion data already loaded; skipping loading.\n');
end

% Define the parameters of the this TPS
parameters;

% Combine both of the above to minimise the amount of changes needed to tps_3D_het
reg_decimations_weights = [reg_decimations; reg_weights];

dose_split = dose / length (beam_angles);

[TBN, TBV] = get_labels ();

% Carbon only (use for testing purposes)
%ion_data_subset = ion_data([4]);
% Carbon-proton
% ion_data_subset = ion_data([1, 4]);
%ion_data_subset = ion_data([1:8]);
% Reverse order - proton last
ion_data_subset = ion_data([1 : 8]);
%head_r, skin_t, skull_t, tumour_r, dose, debug

[phantom, target, oar, hypoxic] = create_spherical_phantom_target_oar ([xpix, ypix, zpix], [centrex, centrey, centrez], head_r, skin_t, skull_t, [tumourx, tumoury, tumourz], tumour_r, TBN.Tumour.value, [oarx, oary, oarz], oar_r, TBN.Brain.value, [hypoxicx, hypoxicy, hypoxicz], hypoxic_r, dose_split);

save phantom.mat phantom
save target.mat target
save oar.mat oar
save hypoxic.mat hypoxic

% Assume equal distribution of dose between angles...

Vt = 0;

t0c = cputime;
t0r = tic;

for idx = 1 : length (beam_angles)
	angle = -beam_angles (idx); % negative as we are rotating the phantom in the opposite direction

	fprintf (stderr, 'Field with %.2f degrees rotation\n', angle);

	if angle == 0
		phantom_rot{idx} = phantom (round (xpix / 2 - xrun / 2) + (1 : xrun), round (ypix / 2 - yrun / 2) + (1 : yrun), round (zpix / 2 - zrun / 2) + (1 : zrun));
		target_rot{idx} = target (round (xpix / 2 - xrun / 2) + (1 : xrun), round (ypix / 2 - yrun / 2) + (1 : yrun), round (zpix / 2 - zrun / 2) + (1 : zrun));
		oar_rot{idx} = oar (round (xpix / 2 - xrun / 2) + (1 : xrun), round (ypix / 2 - yrun / 2) + (1 : yrun), round (zpix / 2 - zrun / 2) + (1 : zrun));
		hypoxic_rot{idx} = hypoxic (round (xpix / 2 - xrun / 2) + (1 : xrun), round (ypix / 2 - yrun / 2) + (1 : yrun), round (zpix / 2 - zrun / 2) + (1 : zrun));
	else
		rotmatx = [1 0 0 0; 0 cosd(angle) sind(angle) 0; 0 -sind(angle) cosd(angle) 0; 0 0 0 1];
		
		tmpphantom = imwarp (phantom, affine3d (rotmatx), 'nearest', 'Fillvalues', TBN.Air.value);
		tmptarget = imwarp (target, affine3d (rotmatx), 'nearest');
		tmpoar = imwarp (oar, affine3d (rotmatx), 'nearest');
		tmphypoxic = imwarp (hypoxic, affine3d (rotmatx), 'nearest');
		
		tmphypoxic = tmphypoxic & tmptarget;
		tmpoar = tmpoar & ~tmptarget;
		
		[xr, yr, zr] = size (tmpphantom);

		phantom_rot{idx} = tmpphantom (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
		target_rot{idx} = tmptarget (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
		oar_rot{idx} = tmpoar (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
		hypoxic_rot{idx} = tmphypoxic (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
	end

	[plan{idx}, D, A, Y] = tps3D_het (phantom_rot{idx}, target_rot{idx}, ion_data_subset, reg_decimations_weights, grid_x, grid_y, grid_z, 'rect', oar_rot{idx}, hypoxic_rot{idx}, hypoxic_threshold_Z);
	V{idx} = calculate_multi_ion_dose_distribution_het (plan{idx});
	plan{idx}.angle = angle;

	angle = -angle;

	if angle == 0
		Vt = Vt + V{idx};
	else
		rotmatx = [1 0 0 0; 0 cosd(angle) sind(angle) 0; 0 -sind(angle) cosd(angle) 0; 0 0 0 1];

		tmp = imwarp (V{idx}, affine3d (rotmatx), 'nearest');

		[xr, yr, zr] = size (tmp);
		tmp2 = zeros (xpix, ypix, zpix);
		tmp2 (round (xpix / 2 - xr / 2) + (1 : xr), round (ypix / 2 - yr / 2) + (1 : yr), round (zpix / 2 - zr / 2) + (1 : zr)) = tmp;

		Vt = Vt + tmp2;
	end
end

Vt = Vt (round (xr / 2 - xpix / 2) + (1 : xpix), round (yr / 2 - ypix / 2) + (1 : ypix), round (zr / 2 - zpix / 2) + (1 : zpix));

t1c = cputime;
elapsed = toc (t0r);

% Compute residual
%residual = sqrt (sum ((V(:) - target(:)) .^ 2));
%save residual.mat residual

save Vt.mat Vt;
save V.mat V;
save -v7.3 plan.mat plan;

delta = t1c - t0c;
times = [delta; elapsed]

save -ascii times.dat times;

% Just for visualisation and calulation of errors...
plan_consolidated = plan{1};
plan_consolidated.phantom_volume = phantom (round (xpix / 2 - xrun / 2) + (1 : xrun), round (ypix / 2 - yrun / 2) + (1 : yrun), round (zpix / 2 - zrun / 2) + (1 : zrun));
plan_consolidated.target_volume = target (round (xpix / 2 - xrun / 2) + (1 : xrun), round (ypix / 2 - yrun / 2) + (1 : yrun), round (zpix / 2 - zrun / 2) + (1 : zrun)) * dose;
plan_consolidated.oar = oar (round (xpix / 2 - xrun / 2) + (1 : xrun), round (ypix / 2 - yrun / 2) + (1 : yrun), round (zpix / 2 - zrun / 2) + (1 : zrun));
plan_consolidated.hypoxic = hypoxic (round (xpix / 2 - xrun / 2) + (1 : xrun), round (ypix / 2 - yrun / 2) + (1 : yrun), round (zpix / 2 - zrun / 2) + (1 : zrun));

[rms_error, D50, D90, PTV, V5, V10, V50] = plan_and_result (plan_consolidated, Vt, [xrun / 2, yrun / 2, zrun / 2]);

save -ascii rms_error.dat rms_error;
D5090 = [D50 D90];
PTV_V5_V10_V50 = [PTV, V5, V10, V50]
save -ascii D5090.dat D5090;
save -ascii dose_volumes.dat PTV_V5_V10_V50;

%Converting TPS output into a macro file to be tested in Geant4  simulation model.
%plan_to_g4mac (plan, library)

fprintf (stderr, 'Elapsed CPU time: %.0f seconds\n', delta);
fprintf (stderr, 'Elapsed real time: %.0f seconds\n', elapsed);
