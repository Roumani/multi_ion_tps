stderr = 2;
map = jet (256);

depths = {'deep', 'central', 'shallow'};

combos = {'no_hypoxia_no_oar', 'yes_hypoxia_no_oar', 'no_hypoxia_yes_oar', 'yes_hypoxia_yes_oar'};

ion_list = {'H_He_O', 'Li_C_Fe', 'all_ions'};
%ion_list = {'all_ions', 'C', 'C_O_Ne', 'Fe', 'H', 'H_C', 'He', 'He_O', 'H_He_O', 'Li', 'Li_C_Fe', 'Li_Ne', 'Ne', 'O', 'Si'};

for d = 1 : length (depths)
	for c = 1 : length (combos)
		for ions = 1 : length (ion_list)
			f = [combos{c} '/' depths{d} '/' ion_list{ions}];
		
			if isdir (f)
				fprintf (stderr, 'Processing plan in %s\n', f);
			
				chdir (f);
% temporary; force regen
				if 1 | ~isfile ('dose_animation.gif') | ~isfile ('isodose_50_animation.gif') | ~isfile ('isodose_90_animation.gif')
					load plan.mat;
					load V.mat;

					plan.phantom_volume (plan.phantom_volume == 1) = 0;
%					plan.phantom_volume (plan.phantom_volume == 3) = 1;
%					plan.phantom_volume (plan.phantom_volume == 4) = 2;
%					plan.phantom_volume (plan.phantom_volume == 5) = 3;
					plan.phantom_volume (plan.phantom_volume == 7) = 0;

					anim (plan, V, 'dose_animation.gif', map);
					anim_isodose (plan, V, 'isodose_50_animation.gif', map, 50);
					anim_isodose (plan, V, 'isodose_90_animation.gif', map, 90);
				else
					fprintf (stderr, 'Animations already exist, skipping\n');
				end

%				close all;
			
				chdir ('../../..');
		
			end
		end
	end
end
