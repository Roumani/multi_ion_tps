function depths_cm = proton_interp (energies)

p = load ("proton_depth_dose.dat");

[v, idx] = sort (p(:, 1));

p = p (idx, :);

depths_cm = interp1 (p(:, 1), p(:, 6), energies)
