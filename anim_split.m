function anim_split (plan, filename_pattern, map)

if nargin < 3
	map = jet;
end

split = split_plan (plan);

vtotal = 0;

for n =  1 : length (split)
	vtotal = vtotal + split{n};
end

cbar = (0:99) / 99 * max (vtotal (:));

%vtotal (:, 1, 1) = cbar;
camproj ('perspective');
%camproj ('orthographic');

N = 500;

if nargin < 3
	filename = 'animation.gif';
end

vec = linspace(0, 4 * pi(), N)';
myPosition = 2 * [zeros(size(vec)) cos(vec) sin(vec)];

nions = length (split);

for n = 1 : nions
	split{n}(1, :, 1) = cbar;
	h = volshow (split{n} + 5 * (plan.phantom_volume) .* (split{n} < 0.01 * max (split{n}(:))) + 5 * plan.oar, 'BackgroundColor', [0 0 0], 'Renderer', 'MaximumIntensityProjection', 'CameraPosition', campos, 'CameraUpVector', [1 0 0], 'ColorMap', map);
	filename = sprintf (filename_pattern, plan.ionlist{n});

	for idx = 1:N
% Update current view.
		h.CameraPosition = myPosition(idx, :);
% Use getframe to capture image.
		I = getframe (gcf);
	
		[indI, cm] = rgb2ind (I.cdata, 256);
% Write frame to the GIF File.
		if idx == 1
			imwrite (indI, cm, filename, 'gif', 'Loopcount', inf, 'DelayTime', 0.08);
		else
			imwrite (indI, cm, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.08);
		end
	end
end
