% Simple metric to calculate what fraction of the target dose volume will be
% occluded by an OAR.  The result should be between 0 (no occlusion) and 1
% (total occlusion).  OAR shouldbe 0 or 1 everywere in the volume, target
% should be the target dose distribution in Gy.

function frac_occluded = dv_occluded (oar, target)

plan_tmp.target_volume = oar;

[regions, ~, ~] = partition_regions (plan_tmp, [1 1 1 1 1 1]);

occluded_target = target .* (regions <= 3);

frac_occluded = sum (occluded_target(:)) / sum (target(:));
