% The optimal dose is constructed by finding the value of vector x which minimises
% the equation
%
% ||Cx - d||^2
%
% Each of the columns of matrix C correspond to a 1D vector of voxels from a
% volume being irradiated by a particular ion at a particular energy at a
% particular beam position (and ONLY that ion/energy/position). Not all
% voxels are used - the voxels are selected by the partition_regions()
% function. If all the voxels are used (theoretically they could be) this
% matrix would have 3 million rows (for a 100x100x300 voxel treatment
% volume) which is computationally unmanageable. Details on how these voxels
% are selected are described in partition_regions.m
%
% d is the desired dose column vector; it has one column the same number of rows as
% C (corresponding to the same selected voxels).
%
% The tps3D function (refer to tps3D.m) solves this to find the optimal
% weight vector x (which has the same number of rows as the number of
% columns in C).

% TODO - look into speedups which may be achievable by
%
% (1) parallelism - seems to be mostly done...?
% (2) using 3D FFT-based convolution instead of explicit summation (maybe this will be much faster)
%
% For now it seems to work.

% debug can be 0 (or unused), 1, 2, or more depending on the level of debug desired

function [C, d] = generate_dose_matrix_het (plan, reg_decimations_weights)

stderr = 2;

% Size of dose distribution data that we have
%
% TODO: If this is too slow, we could prune this down in x and y to make the
% operation faster, at the cost of some peripheral dose error

% Assumption: all ddep volumes are identical in size. This is a reasonable
% assumption BUT maybe later this could be relaxed (e.g. if some
% ions/energies have more spread and we are pruning?)

%[dummy, ddx, ddy, ddz] = size (library{1}.ddep);
[dummy, ddx, ddy, ddz] = size (plan.realdoseprofiles{1}{1});

% Get minimum and maximum beam x and y positions
grid_x_min = min (plan.beamgrid (:, 1));
grid_y_min = min (plan.beamgrid (:, 2));
grid_x_max = max (plan.beamgrid (:, 1));
grid_y_max = max (plan.beamgrid (:, 2));

% Allocate 3D output volume so it is big enough to accomodate the dose
% distributions from the above beams (no need to do this in Z as the beams
% all have the same depth range).

V_template = zeros (max ([(grid_x_max - grid_x_min + 1 + ddx), plan.targ_x]), max ([(grid_y_max - grid_y_min + 1 + ddy), plan.targ_y]), plan.targ_z);

ncoeff = 0;

% How many coefficients do we have? Add them all up.

for beampos = 1 : size (plan.beamgrid, 1)
	for ion = 1 : size (plan.weights{beampos}, 2)
		ncoeff = ncoeff + size (plan.realdoseprofiles{beampos}{ion}, 1);
	end
end

fprintf (stderr, 'Total number of coefficients: %i\n', ncoeff);

% Work out which voxels we want to use in solving for x. reg is the
% partitioned volume (not used currently but nice to have) and selected is
% the list of linearised voxel indices (plan.target_volume is linearised from
% (e.g.) a 100x100x300 matrix to a 3000000x1 vector.

[reg, selected, selected_by_reg] = partition_regions (plan, reg_decimations_weights(1, :));

fprintf (stderr, 'Total number of sampled voxels: %i\n', length (selected));

% Allocate space for matrix C
C = zeros (length(selected), ncoeff);
	
idx = 1;
xc = max ([0, (round (ddx / 2) - grid_x_min)]);
yc = max ([0, (round (ddy / 2) - grid_y_min)]);

fprintf (stderr, 'Populating C-matrix and d-vector...\n');

% Step through each beam position
for beampos = 1 : size (plan.beamgrid, 1)
% Step through each ion
	for ion = 1 : size (plan.weights{beampos}, 2)
%		fprintf (stderr, 'position # %i; ion: %i\n', beampos, ion);
% Step through weights for this ion at this position
		for w = 1 : size (plan.realdoseprofiles{beampos}{ion}, 1)
%			fprintf (stderr, 'profile # %i\n', w);
% Intialise empty volume
			V = V_template;

% Work out where to place the dose distribution		
			x0 = plan.beamgrid (beampos, 1);
			y0 = plan.beamgrid (beampos, 2);

% If the dimensions of the dose volumes in the library exceeds the bounds of
% the original volume we need to treat this a little differently
				
			if ddx / 2 > grid_x_min
				xrange = x0 - grid_x_min + (1 : ddx);
			else
				xrange = x0 - round (ddx / 2) + (1 : ddx);
			end
				
			if ddy / 2 > grid_y_min
				yrange = y0 - grid_y_min + (1 : ddy);
			else
				yrange = y0 - round (ddy / 2) + (1 : ddx);
			end
				
% Copy the dose distribution volume at this location

			V (xrange, yrange, :) = squeeze (plan.realdoseprofiles{beampos}{ion}(w, :, :, :));

% Select the central part that corresponds to the original volume (which
% might be smaller than V)
			ddist = V (xc + (1 : plan.targ_x), yc + (1 : plan.targ_y), :);

% Linearise and put it into the appropriate column of C-matrix
			tmp = ddist(:);

% Apply the weight factors to the different regions for each column in the C-matrix
			for reg_idx = 2 : length (selected_by_reg)
				if reg_decimations_weights (2, reg_idx) > 0
					tmp (selected_by_reg{reg_idx}) = tmp (selected_by_reg{reg_idx}) * reg_decimations_weights (2, reg_idx);
				end
			end
		
			C (:, idx) = tmp (selected);
			idx = idx + 1;
		end
	end
end

% Linearise target volume and retun the subset of voxels over which we are
% optimising; this is the objective for our linear optimiser.
 
d = plan.target_volume (:);

%fprintf (stderr, 'Saving C-matrix.and d-matrix\n');

d = d (selected);
%save -v7.3 C.mat C;
%save -v7.3 d.mat d;

%fprintf (stderr, 'Done.\n');
