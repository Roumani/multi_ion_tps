function spherical_target = create_two_spherical_regions_with_two_doses(xpix_1, ypix_1, zpix_1, centrex_1, centrey_1, centrez_1, radius_1, dose_1, xpix_2, ypix_2, zpix_2, centrex_2, centrey_2, centrez_2, radius_2, dose_2)

%xpix and ypix are in the form of in vector [1:100];

[X_1, Y_1, Z_1] = meshgrid (xpix_1, ypix_1, zpix_1);
[X_2, Y_2, Z_2] = meshgrid (xpix_2, ypix_2, zpix_2);

% Create a spherical target 

spherical_target_1 = ((X_1-centrex_1).^2 + (Y_1-centrey_1).^2 + (Z_1-centrez_1).^2 <= radius_1^2) * dose_1;
spherical_target_2 = ((X_2-centrex_2).^2 + (Y_2-centrey_2).^2 + (Z_2-centrez_2).^2 <= radius_2^2) * dose_2;

spherical_target =spherical_target_1 + spherical_target_2;

%min (spherical_target(:))
%max (spherical_target(:))

% Multiply the spherical region of voxels by a value of a desired dose 

%if nargin == 9 && debug
	figure(1)
	imagesc(squeeze(spherical_target(round (size (spherical_target, 1) / 2), :, :)))
	axis equal;
	colorbar;
	title ('YZ slice');
	figure(2)
	imagesc(squeeze(spherical_target(:, round (size (spherical_target, 2) / 2), :)))
	axis equal;
	colorbar;
	title ('XZ slice');
	figure(3)
	imagesc(squeeze(spherical_target(:, :, round (size (spherical_target, 3) / 2))))
	axis equal;
	colorbar;
	title ('XY slice');
%end
