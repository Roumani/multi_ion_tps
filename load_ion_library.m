function ion_data = load_ion_library ()

% The assumption here is that the folders are structured like this:
% Uncomment the following line if you need to create ion_data library
 ion_names = {'H', 'He', 'Li', 'C', 'O', 'Ne', 'Si', 'Fe'};
% H/100.mat
% H/120.mat
% C/150.mat

% and so on. Basically ion/energy.mat

% First, determine energies. Get a list of all the H energies, load the
% first one, and get its dimensions.

firstion_list = dir (strcat (ion_names{1}, '/*.mat'));
first = load ([ion_names{1} '/' firstion_list(1).name]);

% Get dimensions
[xdim, ydim, zdim] = size (first.data);

unit = 'MeV';

% Iterate through ions
for n = 1 : length (ion_names)
	ion_data{n}.name = ion_names{n};

% Obtain energies for this particular ion - get a list of all the
% files in the folder for this ion
	energies_list =  dir (strcat (ion_names{n}, '/*.mat'));

% Convert to cell array of names
	energies_list = {energies_list.name};

% Strip off .mat
	energies_list = regexprep (energies_list, '.mat', '');
	
% How many do we have?
	n_energies = max (size (energies_list));
	
% Convert text from dir into numbers
	ion_data{n}.energies = sort (sscanf(sprintf(' %s', energies_list{:}), '%f'));

% Create space for dose deposition data
	ion_data{n}.ddep = zeros (n_energies, xdim, ydim, zdim);
	ion_data{n}.maxdosedepth = zeros (n_energies, 1);

% Iterate through energies & load

	for en_idx = 1 : n_energies
% So we can see where we're up to
		fprintf (2, '%s (%i %s)\n', ion_data{n}.name, ion_data{n}.energies(en_idx), unit);

% Have to do this because Matlab is weird
		tmp = load (sprintf ('%s/%i.mat', ion_data{n}.name, ion_data{n}.energies(en_idx)));
		ion_data{n}.ddep (en_idx, :, :, :) = tmp.data;
% Calculate depth of peak dose
		dd = squeeze (sum (sum (tmp.data, 1), 2));
		ion_data{n}.maxdosedepth(en_idx) = find (dd == max (dd));
	end
	
	unit = 'MeV/u';
end

% ion_data is now populated.
%Usage  ion_data = load_ion_library ()
%Unccomment this below line to save ion_data library
save -v7.3 ion_data_all_ions_full_set.mat ion_data; 

end
