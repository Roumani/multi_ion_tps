% Load ion_data if we haven't already done so - this saves a lot of time!

stderr = 2;

if ~exist ('ion_data', 'var')
	fprintf (stderr, 'Loading ion data from current working directory...\n'); 
	ion_data = load_ion_library;
	fprintf (stderr, 'Loading complete.\n')
else
	fprintf (stderr, 'Ion data already loaded; skipping loading.\n');
end

% target, tail, distal, peripheral, proximal, OAR downsample factors
reg_decimations = [1, 1, 25, 200, 25, 1];

% increased this to 3 for speed
%reg_decimations = [3, 3, 25, 200, 25, 3, -1];

% Relative weightings given to the respective regions
reg_weights = [1, 1, 1, 1, 0.01, 10];

% Combine both of the above to minimise the amount of changes needed to tps_3D_het
reg_decimations_weights = [reg_decimations; reg_weights];

% Define the parameters of the this TPS
example_parameters;

% Anything with Z less than or equal to this threshold will not be used in hypoxic regions
hypoxic_threshold_Z = 3;

dose = 100;

[TBN, TBV] = get_labels ();


%[phantom, target] = create_spherical_phantom_target (xpix, ypix, zpix, centrex, centrey, centrez, head_r, skin_t, skull_t, tumour_r, dose);
[phantom, target, oar, hypoxic] = create_spherical_phantom_target_oar ([xpix, ypix, zpix], [centrex, centrey, centrez], head_r, skin_t, skull_t, [tumourx, tumoury, tumourz], tumour_r, TBN.Tumour.value, [oarx, oary, oarz], oar_r, TBN.Brain.value, [hypoxicx, hypoxicy, hypoxicz], hypoxic_r, dose);
%[phantom, target] = create_homogeneous_phantom_target (xpix, ypix, zpix, centrex, centrey, centrez, head_r, skin_t, skull_t, tumour_r, dose);

t0c = cputime;
t0r = tic;

[plan, D, A, Y] = tps3D_het (phantom, target, ion_data_subset, reg_decimations_weights, grid_x, grid_y, grid_z, 'rect', oar, hypoxic, hypoxic_threshold_Z);

t1c = cputime;
elapsed = toc (t0r);

V = calculate_multi_ion_dose_distribution_het (plan);

% Compute residual
residual = sqrt (sum ((V(:) - target(:)) .^ 2));
save residual.mat residual

save V.mat V;
save -v7.3 plan.mat plan;
save target.mat target;

delta = t1c - t0c;
times = [delta; elapsed]

save -ascii times.dat times;
[rms_error, D50, D90, PTV, V5, V10, V50] = plan_and_result (plan, V, [centrex, centrey, centrez]);

save -ascii rms_error.dat rms_error;
D5090 = [D50 D90];
PTV_V5_V10_V50 = [PTV, V5, V10, V50]
save -ascii D5090.dat D5090;
save -ascii dose_volumes.dat PTV_V5_V10_V50;

%Converting TPS output into a macro file to be tested in Geant4  simulation model.
%plan_to_g4mac (plan, library)

fprintf (stderr, 'Elapsed CPU time: %.0f seconds\n', delta);
fprintf (stderr, 'Elapsed real time: %.0f seconds\n', elapsed);
