function [rms_by_reg, D50, D90, PTV, V5, V10, V50] = plan_and_result (plan, result, simresult, cfov)

% Take a slice through the largest cross-section

[regions, idx, partitioned_idx] = partition_regions (plan, [1 1 1 1 1 1; 1 1 1 1 1 1]);

xmaxidx = cfov (1);
ymaxidx = cfov (2);
zmaxidx = cfov (3);

regdef;

regions = regions + plan.hypoxic * hypoxic_val;

for reg_idx = 1 : length (partitioned_idx)
	rms_by_reg(reg_idx) = sqrt (mean ((plan.target_volume (partitioned_idx{reg_idx}) - result(partitioned_idx{reg_idx})) .^ 2));
end

regions = regions + plan.phantom_volume;

figure (1);

ax = subplot (3, 1, 1);
imagesc (squeeze (regions (xmaxidx, :, :)));
xlabel ('Z (depth) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
pbaspect (ax, [3 1 1]);
colorbar;
title ('YZ slice, partitioned phantom');

ax = subplot (3, 1, 2);
imagesc (squeeze (result (xmaxidx, :, :)));
xlabel ('Z (depth) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
pbaspect (ax, [3 1 1]);
colorbar;
title ('YZ slice, planned dose distribution');

ax = subplot (3, 1, 3);
imagesc (squeeze (simresult (xmaxidx, :, :)));
xlabel ('Z (depth) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
pbaspect (ax, [3 1 1]);
colorbar;
title ('YZ slice, achieved dose distribution');

print ('YZ.eps', '-depsc2');
system ('epstopdf YZ.eps && pdftrimwhite YZ.pdf');

%figure (2);

ax = subplot (3, 1, 1);
imagesc (squeeze (regions (:, ymaxidx, :)));
xlabel ('Z (depth) (mm)');
ylabel ('X (horizontal) (mm)')
axis equal;
pbaspect (ax, [3 1 1]);
colorbar
title ('XZ slice, partitioned phantom');

ax = subplot (3, 1, 2);
imagesc (squeeze (result (:, ymaxidx, :)));
xlabel ('Z (depth) (mm)');
ylabel ('X (horizontal) (mm)')
axis equal;
pbaspect (ax, [3 1 1]);
colorbar;
title ('XZ slice, planned dose distribution');

ax = subplot (3, 1, 3);
imagesc (squeeze (simresult (:, ymaxidx, :)));
xlabel ('Z (depth) (mm)');
ylabel ('X (horizontal) (mm)')
axis equal;
pbaspect (ax, [3 1 1]);
colorbar;
title ('XZ slice, achieved dose distribution');

print ('XZ.eps', '-depsc2');
system ('epstopdf XZ.eps && pdftrimwhite XZ.pdf');

% Side view
B = squeeze (sum (plan.target_volume, 1));

zproj = sum (B, 1);

%zmaxidx = round (mean (find (zproj == max (zproj))));

figure (3);

ax = subplot (3, 1, 1);
imagesc (squeeze (regions (:, :, zmaxidx))');
xlabel ('X (horizontal) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
pbaspect (ax, [1 1 1]);
colorbar;

title ('XY slice, partitioned phantom');

ax = subplot (3, 1, 2);
imagesc (squeeze (result (:, :, zmaxidx))');
xlabel ('X (horizontal) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
pbaspect (ax, [1 1 1]);
colorbar;

title ('XY slice, planneded dose distribution');

ax = subplot (3, 1, 3);
imagesc (squeeze (simresult (:, :, zmaxidx))');
xlabel ('X (horizontal) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
pbaspect (ax, [1 1 1]);
colorbar;

title ('XY slice, achieved dose distribution');

print ('XY.eps', '-depsc2');
system ('epstopdf XY.eps && pdftrimwhite XY.pdf');

% Comparison planned plan target dose distributions.

dose_error = result - plan.target_volume;

max (result (:))
min (result (:))

max (plan.target_volume (:))
min (plan.target_volume (:))

max (dose_error (:))
min (dose_error (:))

figure(4)
imagesc (squeeze (dose_error (xmaxidx, :, :)));
xlabel ('Z (depth) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
colorbar

title ('YZ slice, difference of planned and target target dose distributions');
print ('YZ_comparison.eps', '-depsc2');
system ('epstopdf YZ_comparison.eps && pdftrimwhite YZ_comparison.pdf');

figure(5)
imagesc (squeeze (dose_error (:, ymaxidx, :)));
xlabel ('Z (depth) (mm)');
ylabel ('X (horizontal) (mm)')
axis equal;
colorbar

title ('XZ slice, difference of planned and target target dose distributions');
print ('XZ_comparison.eps', '-depsc2');
system ('epstopdf XZ_comparison.eps && pdftrimwhite XZ_comparison.pdf');

figure(6)
imagesc (squeeze (dose_error (:, :, zmaxidx))');
xlabel ('X (horizontal) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
colorbar

title ('XY slice, difference of planned and target target dose distributions');
print ('XY_comparison.eps', '-depsc2');
system ('epstopdf XY_comparison.eps && pdftrimwhite XY_comparison.pdf');

figure (7)

ax = subplot (2, 2, 1);
imagesc (squeeze (regions (:, ymaxidx, :)));
xlabel ('Z (depth) (mm)');
ylabel ('X (horizontal) (mm)')
axis equal;
pbaspect (ax, [3 1 1]);
colorbar;

ax = subplot (2, 2, 2);
imagesc (squeeze (regions (:, :, zmaxidx))');
xlabel ('X (depth) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
pbaspect (ax, [1 1 1]);
colorbar;

ax = subplot (2, 2, 3);
imagesc (squeeze (regions (xmaxidx, :, :)));
xlabel ('Z (depth) (mm)');
ylabel ('Y (vertical) (mm)')
axis equal;
pbaspect (ax, [3 1 1]);
colorbar;

[p, PTV, V5, V10, V50] = Dxx ([50, 90], plan.target_volume, result, plan.phantom_volume)
D50 = p (1);
D90 = p (2);
