function [phantom, target, densitymap] = create_spherical_phantom_target (xpix, ypix, zpix, centrex, centrey, centrez, head_r, skin_t, skull_t, tumour_r, dose, debug)

[TBN, TBV] = get_labels ();

%xpix and ypix are in the form of in vector [1:100];

[X, Y, Z] = meshgrid (xpix, ypix, zpix);

% Create a spherical target 

target = ((X-centrex).^2 + (Y-centrey).^2 + (Z-centrez).^2 <= tumour_r^2);

head = ((X-centrex).^2 + (Y-centrey).^2 + (Z-centrez).^2 <= head_r^2);
skull = ((X-centrex).^2 + (Y-centrey).^2 + (Z-centrez).^2 <= (head_r - skin_t) ^2);
brain = ((X-centrex).^2 + (Y-centrey).^2 + (Z-centrez).^2 <= (head_r - skin_t - skull_t)^2);

phantom = head * TBN.Skin.value + (~head) * TBN.Air.value;
phantom (skull - brain > 0) = TBN.Bone.value;
phantom (brain - target > 0) = TBN.Brain.value;
phantom (target > 0) = TBN.Tumour.value;

target = target * dose;

%min (spherical_target(:))
%max (spherical_target(:))

% Multiply the spherical region of voxels by a value of a desired dose 

if nargin == 12 && debug
	figure(1)
	imagesc(squeeze(target(round (size (target, 1) / 2), :, :)))
	axis equal;
	colorbar;
	figure(2)
	imagesc(squeeze(target(:, round (size (target, 2) / 2), :)))
	axis equal;
	colorbar;
	figure(3)
	imagesc(squeeze(target(:, :, round (size (target, 3) / 2))))
	axis equal;
	colorbar;

	figure(4)
	imagesc(squeeze(phantom(round (size (phantom, 1) / 2), :, :)))
	axis equal;
	colorbar;
	figure(5)
	imagesc(squeeze(phantom(:, round (size (phantom, 2) / 2), :)))
	axis equal;
	colorbar;
	figure(6)
	imagesc(squeeze(phantom(:, :, round (size (phantom, 3) / 2))))
	axis equal;
	colorbar;
end

materials = unique (phantom(:))';

densitymap = phantom;

for m = materials
	densitymap (phantom == m) = TBV(m).density;
end
