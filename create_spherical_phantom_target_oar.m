% Tumour and OAR coordinates are relative to the centre of the head. The head is then positioned such that the tumour is placed at centre_coords_px. 

function [phantom, target, oar, hypoxic] = create_spherical_phantom_target_oar (dimensions_px, centre_coords_px, head_r, skin_t, skull_t, tumour_coords, tumour_r, tumour_tissue, oar_coords, oar_r, oar_tissue, hypoxic_coords, hypoxic_r, dose, debug)

xpix = dimensions_px (1);
ypix = dimensions_px (2);
zpix = dimensions_px (3);

centrex = centre_coords_px (1);
centrey = centre_coords_px (2);
centrez = centre_coords_px (3);

[TBN, TBV] = get_labels ();

[Y, X, Z] = meshgrid (1:ypix, 1:xpix, 1:zpix);

% Create a spherical target 

% Tumour is always at specified centre point.

target = (X - centrex).^2 + (Y - centrey).^2 + (Z - centrez).^2 <= tumour_r^2;
head = (X - centrex + tumour_coords(1)).^2 + (Y - centrey + tumour_coords(2)).^2 + (Z - centrez + tumour_coords(3)).^2 <= head_r^2;
skull = (X - centrex + tumour_coords(1)).^2 + (Y - centrey + tumour_coords(2)).^2 + (Z - centrez + tumour_coords(3)).^2 <= (head_r - skin_t) ^2;
brain = (X - centrex + tumour_coords(1)).^2 + (Y - centrey + tumour_coords(2)).^2 + (Z - centrez + tumour_coords(3)).^2 <= (head_r - skin_t - skull_t)^2;

% Region tagged as organs at risk (OAR)
oar = (X - centrex + tumour_coords(1) - oar_coords(1)).^2 + (Y - centrey + tumour_coords(2) - oar_coords(2)).^2 + (Z - centrez + tumour_coords(3) - oar_coords(3)).^2 <= oar_r^2;

% Make sure there is no overlap between OAR and target
oar = oar & ~target;

phantom = head * TBN.Skin.value + (~head) * TBN.Air.value;
phantom (skull - brain > 0) = TBN.Bone.value;
phantom (brain - target > 0) = TBN.Brain.value;
phantom (target > 0) = tumour_tissue;
phantom (oar > 0) = oar_tissue;

target = target * dose;

hypoxic = (X - centrex + tumour_coords(1) - hypoxic_coords(1)).^2 + (Y - centrey + tumour_coords(2) - hypoxic_coords(2)).^2 + (Z - centrez + tumour_coords(3) - hypoxic_coords(3)).^2 <= hypoxic_r^2;

% Make sure hypoxic region is inside target.
hypoxic = hypoxic & target;

save hypoxic.mat hypoxic;
save oar.mat hypoxic;
save phantom.mat phantom;
save target.mat target;
