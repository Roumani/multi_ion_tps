function percent_contrib = contributions (plan)

stdout = 1;

split = split_plan (plan);

vtotal = 0;

for n =  1 : length (split)
	vtotal = vtotal + split{n};
end

target_dose = vtotal .* plan.target_volume;

dosevolume = sum (target_dose (:));

nions = length (split);

for n = 1 : nions
	ion_dose = split{n} .* plan.target_volume;
	ion_dosevolume = sum (ion_dose (:));
	
	percent_contrib(n) = ion_dosevolume / dosevolume * 100;
	fprintf (stdout, '%s\t', plan.ionlist{n});
end

fprintf (stdout, '\n');
percent_contibution = [];
for n = 1 : nions
	fprintf (stdout, '%.2f\t', percent_contrib(n));
	percent_contibution = [percent_contibution percent_contrib(n)];


end

save -ascii ion_contribution.dat percent_contibution;

fprintf (stdout, '\n');
