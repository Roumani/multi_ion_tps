% target_volume should be a 3D voxelised volume with nonzero values for wherever we want some dose deposited.
%
% library should be a cell array of ion structures, which have the following elements:
%
% .name (e.g. 'H', 'Li' etc.)
% .energies (in MeV/u, e.g. [150 175 ... ])
% .ddata (4D volumetric dose distribution array; [energy_idx, x, y, z])
% .maxdepthdose (voxel depth of peak)
%
% decimations should be a vector of four numbers used to weight the cost function: [target behind around infront]
%
% these numbers do not need to be normalised.
%
% grid_x and grid_y are the steps (in voxels) between the raster scan steps in x and y, respectively
%
% latticetype is an optional argument; it can be either 'rect' or 'tri'. If
% 'tri' then grid_y is ignored and the beamgrid is an equilateral triangular
% lattice.
%
% TODO: think about scaling to mm for various things (?)

function [plan, C, d, x, resnorm, residual] = tps3D_het (phantom_volume, target_volume, library, reg_decimations_weights, grid_x, grid_y, grid_z, latticetype, oar, hypoxic, hypoxic_threshold_Z)

stderr = 2;

% Sanity checks: is the OAR volume overlapping with the target volume?

N = sum (target_volume (:) & oar (:));

if N
	error ('Organ at risk (OAR) overlaps with target volume. Aborting.');
end

% Is the hypoxic region outside of the target volume?

N = sum (~target_volume (:) & hypoxic (:));

if N
	error ('Hypoxic region extends beyond target volume. Aborting.');
end


% TODO: change this if needed
voxel_depth_mm = 1;
overdose = 1;

% First step: segment the volume into four subsets:

% 1. The target itself
% 2. In front of the target
% 3. Behind the target
% 4. Around the target
%
% The objective of the TPS is to achieve the planned dose within the target,
% with minimum dose behind and around the target (which can feasibly be
% near-zero) and minimum dose in front of the target (although this is much
% less important than reaching the planned dose within the target, and it is
% understood that this cannot be zero).

% Work out the dimensions of the target

[targ_x, targ_y, targ_z] = size (target_volume);

% Flatten Z axis to create XY projection - determine range of X, Y where we need to apply some dose

xy_plane = squeeze (sum (target_volume, 3));

% These are the full list of x and y indices in the raster-scan mesh; these get re-used quite a bit

fprintf (stderr, 'Lattice type is %s\n', latticetype);

if nargin < 8 || (nargin >= 8 && strcmp (latticetype, 'rect'))
	raster_x = 1 : grid_x : targ_x;
	raster_y = 1 : grid_y : targ_y;
elseif strcmp (latticetype, 'tri')
	raster_x = round (1 : (grid_x / 2) : targ_x);
	raster_y = round (1 : (grid_x * sqrt(3) / 2) : targ_y);
else
	error ('Unknown lattice type (should be rect or tri)');
end

% Generate subset of full resolution XY plane where we have some dose (0 or 1; 1 = member of set)
targ_xy_reg = xy_plane > 0;

% Of the points in our mesh, which are in each subset? This is a truth value (0 or 1)
targ_mask = targ_xy_reg (raster_x, raster_y);

% What are the index pairs of these?

% First the target - these are all the X, Y coordinates where we will be
% aiming the beam (with different ions/energies in each case). The Z
% placeholders are for the min & max depth for each XY pair, which we will calculate next.

[xi, yi] = find (targ_mask);
targ_beam_coord_list = [raster_x(xi)', raster_y(yi)', zeros(length(xi), 5)];

% Adjusted PMMA-equivalent depth
dprime = zeros (length(xi), targ_z);
% Amplitude scaling factor
ampscale = dprime;

if strcmp (latticetype, 'tri')
	xodd = raster_x (1:2:end);
	xeven = raster_x (2:2:end);
	yodd = raster_y (1:2:end);
	yeven = raster_y (2:2:end);

	targ_beam_coord_list ((sum ((targ_beam_coord_list (:, 1) == xeven), 2) & sum ((targ_beam_coord_list (:, 2) == yodd), 2)) | (sum ((targ_beam_coord_list (:, 1) == xodd), 2) & sum ((targ_beam_coord_list (:, 2) == yeven), 2)), :) = [];
end

% And for the full-resolution mask - this will be used for segmenting the
% volume for the evaluation of the quality of the treatment plan.

% OK, now what are the depth ranges for our target for each of the X, Y?
% This is how we will determine the subset of ions/energies that can
% potentially be used for each X, Y point.

% How many raster points?
n_raster_points = size (targ_beam_coord_list, 1);

% Get a list of the materials comprising this phantom
%materials = unique (phantom);
[TBN, TBV] = get_labels ();

% Go through all of them and find the depth range
% Later: add the corresponding energy range for each ion species that we can use for each point (?)

for beampos = 1 : n_raster_points
	nz = find (target_volume (targ_beam_coord_list (beampos, 1), targ_beam_coord_list (beampos, 2), :));
	targ_beam_coord_list (beampos, 3) = min (nz);
	targ_beam_coord_list (beampos, 4) = max (nz);
	
	hyp_nz = find (hypoxic (targ_beam_coord_list (beampos, 1), targ_beam_coord_list (beampos, 2), :));
	
	if length (hyp_nz) 
		fprintf (stderr, 'hypoxic region onset detected in position %i\n', beampos);
		targ_beam_coord_list (beampos, 7) = min (nz);
	end

	matlist = [];
	thicklist = [];
	
	prevmat = -1;

	dprime1 = 0;
	dprime2 = 0;
	
	running_depth = 0;

	for z = 1 : targ_z
		mat = phantom_volume (targ_beam_coord_list (beampos, 1), targ_beam_coord_list (beampos, 2), z);
		
		if mat ~= prevmat
			matlist (end + 1) = mat;
			thicklist (end + 1) = 1;

			if prevmat == -1
				pm = 1;
			else
				pm = prevmat;
			end

% If we have entered a tumour region
			if mat == TBN.Tumour.value				
				for k = 1 : (length (matlist) - 1)
%					fprintf (stderr, 'k = %i, matlist(k) = %s, thicklist(k) = %i\n', k, TBV(matlist(k)).Name, thicklist(k));
					
					dprime1 = dprime1 + pmma_equiv_depth (matlist (k), thicklist (k), TBV);
				end
			elseif prevmat == TBN.Tumour.value
% If we have left a tumour region
				dprime2 = dprime1 + pmma_equiv_depth (matlist (k), thicklist (k), TBV);
			end
		else
			thicklist (end) = thicklist (end) + 1;		
		end
		
% TODO: think about half-voxel offset issues
% dprime is the equivalent depth in PMMA for this ion/energy at this position in the phantom (should generally be SHALLOWER than z).
		
		dprime (beampos, z) = running_depth + voxel_depth_mm * TBV (mat).PERR;
		ampscale (beampos, z) = TBV (mat).PERR;
		running_depth = dprime (beampos, z);
		
		prevmat = mat;
	end
	
	targ_beam_coord_list (beampos, 5) = dprime1;
	targ_beam_coord_list (beampos, 6) = dprime2;
end

% Dimensions of dose dist data in voxels (TODO: change to mm at some point)
[~, xdim, ydim, zdim] = size (library{1}.ddep(1, :, :, :));

plan.beamgrid = targ_beam_coord_list;
plan.ionlist = {cell2mat(library).name};

fprintf (stderr, 'Generating local beam profile set...\n');

depthgrid = 1 : grid_z : zdim;

% Iterate through each beam position
for beampos = 1 : size (targ_beam_coord_list, 1)
% Iterate through all possible ion species
	for ion = 1 : length (library)
% Depths at which max dose is deposited for each energy for this ion species
		dds = library{ion}.maxdosedepth; % depths of peaks in PMMA

		k = 1;
		
		if overdose
			depths = depthgrid (depthgrid >= targ_beam_coord_list (beampos, 3) & depthgrid <= targ_beam_coord_list (beampos, 4));
		else
			depths = depthgrid (depthgrid > targ_beam_coord_list (beampos, 3) & depthgrid < targ_beam_coord_list (beampos, 4));
		end
		
%		fprintf (stderr, 'Beampos %i (%i, %i), ion %i: # profiles = %i\n', beampos, targ_beam_coord_list (beampos, 1), targ_beam_coord_list (beampos, 2), ion, length (depths));
%		fprintf (stderr, 'Actual depth range: %.0f-%.0f\n', targ_beam_coord_list (beampos, 3), targ_beam_coord_list (beampos, 4));
%		fprintf (stderr, 'Adjusted depth range: %.0f-%.0f\n', targ_beam_coord_list (beampos, 5), targ_beam_coord_list (beampos, 6));
		
		plan.realdoseprofiles{beampos}{ion} = zeros (length (depths), xdim, ydim, zdim);
		plan.energies{beampos}{ion} = zeros (length (depths), 1);
		plan.weights{beampos}{ion} = zeros (length (depths), 1);

% Iterate through depth range of tumour in steps of grid_z
		for d = depths
% If a hypoxic range is defined for this position, and we are in the hypoxic range, and we are lower-Z than the hypoxic Z threshold, then we are done for this ion.
			if targ_beam_coord_list (beampos, 7) & d >= targ_beam_coord_list (beampos, 7) & library{ion}.Z <= hypoxic_threshold_Z
				fprintf (stderr, 'In hypoxic region: position %i at depth %.2f : not using ion %s any further\n', beampos, d, library{ion}.name);
				break;
			end

% Find which energy of this ion will deposit max energy at the desired depth
			tmp = abs (dds - dprime (beampos, d));
			idx = find (tmp == min (tmp));
			idx = idx(1);
			energy = library{ion}.energies(idx);
%			fprintf (stderr, '@depth %.2f, Energy = %.2f MeV/u\n', d, energy);
			
% Generate the actual dose profile for this energy and add it to the list
			v = squeeze (library{ion}.ddep(idx(1), :, :, :));

			[x2, y2, z2] = meshgrid (1 : xdim, 1 : ydim, dprime (beampos, :));

			tmp = interp3 (v, x2, y2, z2, 'linear', 0);
			tmp2 = reshape (tmp, xdim * ydim, []);			

			tmp3 = reshape (tmp2 .* ampscale (beampos, :), xdim, ydim, zdim);

			plan.realdoseprofiles{beampos}{ion}(k, :, :, :) = tmp3;
			plan.energies{beampos}{ion}(k) = energy;

			k = k + 1;
		end
	end
% What materials do we have in this section?
end

fprintf (stderr, 'done.\n');

plan.target_volume = target_volume;
plan.phantom_volume = phantom_volume;

if nargin >= 9
	plan.oar = oar;
end

if nargin >= 10
	plan.hypoxic = hypoxic;
end

plan.targ_x = targ_x;
plan.targ_y = targ_y;
plan.targ_z = targ_z;

[C, d] = generate_dose_matrix_het (plan, reg_decimations_weights);

% Find x such that ||Cx - d||^2 is minimised

fprintf (stderr, 'Starting non-negative least squares optimisation...\n');

%options = optimoptions ('lsqnonneg', 'TolX', 1000, 'Display', 'iter');

[x, resnorm, residual] = lsqnonneg (C, d);

fprintf (stderr, 'Done\n');

% Now for ease of reading we transfer the solution back into the treatment plan struture

fprintf (stderr, 'Copying weights to plan...\n');

n = 1;

for beampos = 1 : size (plan.beamgrid, 1)
% Step through each ion
	for ion = 1 : size (plan.weights{beampos}, 2)
% Step through weights for this ion at this position
		for w = 1 : size (plan.realdoseprofiles{beampos}{ion}, 1)
			plan.weights{beampos}{ion}(w) = x(n);
			n = n + 1;
		end
	end
end

fprintf (stderr, 'Done\n');
