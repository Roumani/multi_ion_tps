function  bio_dose = phys2bio ()

load ion_data_small_six_ions.mat; 
ion_names = {'H', 'He', 'Li', 'C', 'O', 'Ne'};

for ion = 1 : length(ion_names)

rbe_filename  = dir (strcat (ion_names{ion}, '/*.mat'));
rbe = load ([ion_names{ion} '/' rbe_filename.name]);


for e_idx = 1 : length (ion_data{ion}.energies)
    
tmp = permute (squeeze(ion_data{ion}.ddep(e_idx,:,:,:)),[3,2,1]) .* rbe.vec_new_matrix(:,e_idx);
ion_data{ion}.ddep(e_idx,:,:,:) = permute (tmp, [3 2 1]);

%ion_data{ion}.ddep(e_idx,:,:,:) = tmp;

end

% Save the output 
save -v7.3 ion_data_bio_dose.mat ion_data; 

end



%tmp = permute (squeeze(ion_data{4}.ddep(68,:,:,:)),[3,2,1]) .* rbe.vec_new_matrix(:,68);
%tmp = permute (tmp, [3 2 1]);

