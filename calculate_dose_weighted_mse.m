% After doing some performance profiling, we may de-functionalise this to
% speed things up if necessary.

function wmse = calculate_dose_weighted_mse (measured_ddist, target_ddist, weights)

targ_val = 1;
distal_val = 2;
around_val = 3;
proximal_val = 4;

regions = zeros (size (target_ddist));
xy_plane = squeeze (sum (target_ddist, 3));

% Generate subset of full resolution XY plane where we have some dose (0 or 1; 1 = member of set)
targ_xy_reg = (xy_plane > 0);
[targ_x, targ_y] = find (targ_xy_reg);
n_targ_points = length (targ_x);

targ_zrange = zeros (n_targ_points, 2);

% Tag target, proximal and distal regions (use labels defined above to label a volume called 'regions')
for r = 1 : n_targ_points
	nz = find (target_ddist (targ_x(r), targ_y(r), :));
	targ_zrange (r, 1) = min (nz);
	targ_zrange (r, 2) = max (nz);
	regions (targ_x(r), targ_y(r), targ_zrange (r, 1) : targ_zrange (r, 2)) = targ_val;
	regions (targ_x(r), targ_y(r), 1 : targ_zrange (r, 1) - 1) = proximal_val;
	regions (targ_x(r), targ_y(r), targ_zrange (r, 2) + 1 : end) = distal_val;
end

% Generate subset of full resolution XY plane where we have zero dose (0 or 1; 1 = member of set)
around_xy_reg = (xy_plane == 0);
[around_x, around_y] = find (around_xy_reg);
n_around_points = length (around_x);

% Tag the around-region (laterally peripheral to target)
for r = 1 : n_around_points
	regions (around_x (r), around_y (r), :) = around_val;
end

% Calculate squared error across whole volume
dsq = (target_ddist - ddist) .^ 2;

% Extract each sub-region from the squared error volume
targ_dsq = dsq (regions == targ_val);
distal_dsq = dsq (regions == distal_val);
around_dsq = dsq (regions == around_val);
proximal_dsq = dsq (regions == proximal_val);

% Calculate mean of each region and then compute weighted sum of those; normalise by sum of weights
wmse = (weights (1) * sum (targ_dsq (:)) + weights (2) * sum (distal_dsq (:)) + weights (3) * sum (around_dsq (:)) + weights (4) * sum (proximal_dsq (:))) / sum (weights) * length (dsq (:));

% Get rid of this when we are happy with it
imagesc (squeeze (regions (:, 50, :)));
