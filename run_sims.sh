#!/bin/sh


# Run as run_sims.sh homo or run_sims.sh het
# Set default
HIT=hit_het

# If we have provided an argument
if [ x$1 != x ] ; then
# And it is homo
	if [ $1 = 'homo' ] ; then
		HIT=hit_homo
	elif [ $1 = 'het' ] ; then
		HIT=hit_het
	elif [ $1 = 'uts' ] ; then
		HIT=hit_uts
	fi
fi 

echo hit simulation binary is $HIT

for ion in H He Li C Fe O Ne Si ; do
#for ion in Fe ; do
	if [ -d $ion ] ; then
		cd $ion

		for pos in P_* ; do
			cd  $pos
		
			for en in * ; do
				if [ -d $en ] ; then
					cd $en
						~/MRE/hit_let/build/$HIT run.mac
					cd ..
				fi
			done
	
			cd ..
		done
	
		cd ..
	fi
done

# Adding a post-processing step to merge all the resulting root files
#hadd HiT.root */*/*/HiT.root

