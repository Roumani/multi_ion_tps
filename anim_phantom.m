function anim_phantom (plan, filename, map)

if nargin < 3
	map = jet;
end

h = volshow (plan.phantom_volume + plan.hypoxic + plan.oar, 'BackgroundColor', [0 0 0], 'Renderer', 'MaximumIntensityProjection', 'CameraPosition', [2 2 0], 'CameraUpVector', [1 0 0], 'ColorMap', map);

camproj ('perspective');
%camproj ('orthographic');

N = 500;

if nargin < 2
	filename = 'animation.gif';
end

vec = linspace(0, 4 * pi(), N)';
myPosition = 2 * [zeros(size(vec)) cos(vec) sin(vec)];

for idx = 1:N
% Update current view.
	h.CameraPosition = myPosition(idx, :);
% Use getframe to capture image.
	I = getframe (gcf);
	
	[indI, cm] = rgb2ind (I.cdata, 256);
% Write frame to the GIF File.
	if idx == 1
		imwrite (indI, cm, filename, 'gif', 'Loopcount', inf, 'DelayTime', 0.05);
	else
		imwrite (indI, cm, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.05);
	end
end
