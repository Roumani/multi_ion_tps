% TODO - look into big speedups which may be achievable by using 3D
% FFT-based convolution instead of explicit summation (maybe this will be
% much faster)
%
% debug can be 0 (or unused), 1, 2, or more depending on the level of debug desired

function ddist = calculate_multi_ion_dose_distribution_het (plan, debug)

stderr = 2;

% Size of dose distribution data that we have
%
% TODO: If this is too slow, we could prune this down in x and y to make the
% operation faster, at the cost of some peripheral dose error

% Assumption: all ddep volumes are identical in size. This is a reasonable
% assumption BUT maybe later this could be relaxed (e.g. if some
% ions/energies have more spread and we are pruning?)

[dummy, ddx, ddy, ddz] = size (plan.realdoseprofiles{1}{1});

% Get minimum and maximum beam x and y positions
grid_x_min = min (plan.beamgrid (:, 1));
grid_y_min = min (plan.beamgrid (:, 2));
grid_x_max = max (plan.beamgrid (:, 1));
grid_y_max = max (plan.beamgrid (:, 2));

% Allocate 3D output volume so it is big enough to accomodate the dose
% distributions from the above beams (no need to do this in Z as the beams
% all have the same depth range).

V = zeros (max ([(grid_x_max - grid_x_min + 1 + ddx), plan.targ_x]), max ([(grid_y_max - grid_y_min + 1 + ddy), plan.targ_y]), plan.targ_z);

% Step through each beam position
for beampos = 1 : size (plan.beamgrid, 1)
	if nargin == 3 && debug > 0
		fprintf (stderr, 'Beam position %i: %i, %i\n', beampos, plan.beamgrid(beampos, 1), plan.beamgrid(beampos, 2))
	end

% Step through each ion
	for ion = 1 : size (plan.weights{beampos}, 2)
		if nargin == 2 && debug > 1
			fprintf (stderr, 'Contribution from ion #%i\n', ion);
		end
		
% Step through weights for this ion at this position
		for w = 1 : size (plan.realdoseprofiles{beampos}{ion}, 1)
% Add energy contribution if weight is positive (this takes care of
% optimisation algorithms which can produce negative coefficients and which
% do not allow explicit constraints)

			if nargin == 2 && debug > 2
				if strcmp (plan.ionlist{ion}, 'H')
					unit = 'MeV';
				else
					unit = 'MeV/u';
				end

				fprintf (stderr, 'Contribution from energy %i %s\n', library{ion}.energies(w), unit);	
			end
	
			x0 = plan.beamgrid (beampos, 1);
			y0 = plan.beamgrid (beampos, 2);
				
			if ddx / 2 > grid_x_min
				xrange = x0 - grid_x_min + (1 : ddx);
			else
				xrange = x0 - round (ddx / 2) + (1 : ddx);
			end
				
			if ddy / 2 > grid_y_min
				yrange = y0 - grid_y_min + (1 : ddy);
			else
				yrange = y0 - round (ddy / 2) + (1 : ddx);
			end

%			min(xrange)
%			max(xrange)
%			min(yrange)
%			max(yrange)

			V (xrange, yrange, :) = V (xrange, yrange, :) + plan.weights{beampos}{ion}(w) * squeeze (plan.realdoseprofiles{beampos}{ion}(w, :, :, :));
		end
	end
end

x0 = max ([0, (round (ddx / 2) - grid_x_min)]);
y0 = max ([0, (round (ddy / 2) - grid_y_min)]);

ddist = V (x0 + (1 : plan.targ_x), y0 + (1 : plan.targ_y), :);
