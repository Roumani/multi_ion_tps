% Example usage: dosevis (plan, -60, jet(256))

function dosevis (plan, theta, map)

map = jet (256);

stderr = 2;

% Force plan to be a cell array, even if it is just a single field

if ~iscell (plan)
	if ~isfield (plan, 'angle')
		plan.angle = 0;
	end

	tmp{1} = plan;
	plan = tmp;
end

campos = [0.5, 1*cosd(theta) + 1, 1*sind(theta)] + [0 0 -0.1];
camtarg = -[0.5, 1*cosd(theta) + 1, 1*sind(theta)] + [0 0 -0.1];%[-0.5, -0.5, 0];

phvals = unique (plan{1}.phantom_volume)';

xrun = plan{1}.targ_x;
yrun = plan{1}.targ_y;
zrun = plan{1}.targ_z;

% intensity of phantom and OAR and should be 0.05 (5%) of peak dose

bg_intensity = 0.3;

xpad = 450;
ypad = 100;
zpad = 450;

[TBN, TBV] = get_labels ();

close all;

nfields = length (plan);

%cbar = (0:99) / 99 * max (vtotal{pidx} (:));

cbarmap = jet (50);
NC = size (cbarmap, 1);

for pidx = 1 : nfields
	fprintf (stderr, 'Field %i (%2f degrees)\n', pidx, plan{pidx}.angle);
	split = split_plan (plan{pidx});
	nions = length (split);

	angle = -plan{pidx}.angle;

	rotmatx = [1 0 0 0; 0 cosd(angle) sind(angle) 0; 0 -sind(angle) cosd(angle) 0; 0 0 0 1];

	vtotal{pidx} = 0;

	pad = zeros (xpad, ypad, zpad);

% pad with zeros / ones

	tmptarget = pad;
	tmpoar = pad;
	tmphypoxic = pad;
	tmpphantom = ones (size (tmptarget)) * TBN.Air.value;

% and copy to centre

	tmptarget (round (xpad / 2 - xrun / 2) + (1 : xrun), round (ypad / 2 - yrun / 2) + (1 : yrun), round (zpad / 2 - zrun / 2) + (1 : zrun)) = plan{pidx}.target_volume;
	tmpoar (round (xpad / 2 - xrun / 2) + (1 : xrun), round (ypad / 2 - yrun / 2) + (1 : yrun), round (zpad / 2 - zrun / 2) + (1 : zrun)) = plan{pidx}.oar;
	tmphypoxic (round (xpad / 2 - xrun / 2) + (1 : xrun), round (ypad / 2 - yrun / 2) + (1 : yrun), round (zpad / 2 - zrun / 2) + (1 : zrun)) = plan{pidx}.hypoxic;
	tmpphantom (round (xpad / 2 - xrun / 2) + (1 : xrun), round (ypad / 2 - yrun / 2) + (1 : yrun), round (zpad / 2 - zrun / 2) + (1 : zrun)) = plan{pidx}.phantom_volume;

% rotate

	tmptarget = round (imwarp (tmptarget * 1, affine3d (rotmatx), 'bilinear'));
	tmpoar = round (imwarp (tmpoar * 1, affine3d (rotmatx), 'bilinear'));
	tmphypoxic = round (imwarp (tmphypoxic * 1, affine3d (rotmatx), 'bilinear'));

% phantom is a bit trickier

	tmpphantom2 = 0;
		
	for phval = phvals
		tmp = (tmpphantom == phval) * 1.0; % binary 0 or 1 for that part of the phantom
		tmp = round (imwarp (tmp, affine3d (rotmatx), 'bilinear')); % should also be binary
		[xr, yr, zr] = size (tmp);
		tmp = tmp (round (xr / 2 - xpad / 2) + (1 : xpad), round (yr / 2 - ypad / 2) + (1 : ypad), round (zr / 2 - zpad / 2) + (1 : zpad));
		tmpphantom2 = (tmpphantom2 .* (tmp == 0)) + tmp * phval;
	end

% trim for visualisation

	[xr, yr, zr] = size (tmpphantom2);

	phantom_rot{pidx} = tmpphantom2 (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));

	[xr, yr, zr] = size (tmptarget);

	target_rot{pidx} = tmptarget (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
	oar_rot{pidx} = tmpoar (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
	hypoxic_rot{pidx} = tmphypoxic (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));

	for n = 1 : nions
		tmpsplit = pad;
		tmpsplit (round (xpad / 2 - xrun / 2) + (1 : xrun), round (ypad / 2 - yrun / 2) + (1 : yrun), round (zpad / 2 - zrun / 2) + (1 : zrun)) = split{n};
		tmpsplit = imwarp (tmpsplit, affine3d (rotmatx), 'bilinear');
		split_rot{n} = tmpsplit (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
		vtotal{pidx} = vtotal{pidx} + 1.0 * split_rot{n};
	end

	view_panel = uipanel (figure (1));
	view_panel.FontSize = 14;
	view_panel.TitlePosition = 'centertop';

	Vtf = vtotal{pidx};

	max_dose = max (Vtf(:));
	max_oar = max (oar_rot{pidx}(:));
	max_phantom = max (phantom_rot{pidx}(:));

	if max_dose < max_phantom
		Vtf (1, 1, 1) = 100;
		max_dose = 100;
	end

	if max_oar == 0
		max_oar = 1;
	end
	
	background = (phantom_rot{pidx} .* (1.0 * oar_rot{pidx} < 0.1) / max_phantom + 1.0 * oar_rot{pidx} / max_oar) * max_dose * bg_intensity;

	h = volshow (Vtf .* (Vtf >= 0.01 * max_dose) + background .* (Vtf < 0.01 * max_dose), 'BackgroundColor', [0 0 0], 'Renderer', 'MaximumIntensityProjection', 'CameraPosition', campos, 'CameraUpVector', [1 0 0], 'ColorMap', map, 'CameraTarget', camtarg, 'Parent', view_panel);

	ax = axes ('Parent', view_panel, 'Units','normalized', 'Position', [0.95 0 0.05 1], 'Visible', 'off', 'YLim', [0 NC], 'CLim', [0 NC], 'Colormap', cbarmap);
	p = patch ('Parent', ax, 'XData', repmat ([0; 1; 1; 0], 1, NC), 'YData', [0; 0; 1; 1] + (0 : (NC - 1)), 'FaceColor', 'flat', 'FaceVertexCData', ax.Colormap, 'LineStyle', 'None');
	dmax = max (Vtf(:));
	t = text (ax, -0.5 * ones (1, 10), ((1 : 10) - 0.5) / 10 * NC, sprintfc ('%.0f', (0 : 9) / 9 * dmax), 'HorizontalAlignment', 'center', 'Color', [1 1 1]);

	I = getframe (gcf);
	imwrite (I.cdata, sprintf ('totaldose_%i.png', pidx), 'png');
	
	[xd, yd, zd] = size (vtotal{pidx});
	
	for n = 1 : nions
		clf;
		view_panel = uipanel (figure (1));
		view_panel.FontSize = 14;
		view_panel.TitlePosition = 'centertop';

		max_dose = max (split_rot{n}(:));

		if max_dose < max_phantom
			split_rot{n}(1, 1, 1) = 100;
			max_dose = 100;
		end

		background = (phantom_rot{pidx} .* (oar_rot{pidx} < 0.1) / max_phantom + 1 * oar_rot{pidx} / max_oar) * max_dose * bg_intensity;

		h = volshow (split_rot{n} .* (split_rot{n} >= 0.01 * max_dose) + background .* (split_rot{n} < 0.01 * max_dose), 'BackgroundColor', [0 0 0], 'Renderer', 'MaximumIntensityProjection', 'CameraPosition', campos, 'CameraUpVector', [1 0 0], 'ColorMap', map, 'CameraTarget', camtarg, 'Parent', view_panel);
		ax = axes ('Parent', view_panel, 'Units','normalized', 'Position', [0.95 0 0.05 1], 'Visible', 'off', 'YLim', [0 NC], 'CLim', [0 NC], 'Colormap', cbarmap);
		p = patch ('Parent', ax, 'XData', repmat ([0; 1; 1; 0], 1, NC), 'YData', [0; 0; 1; 1] + (0 : (NC - 1)), 'FaceColor', 'flat', 'FaceVertexCData', ax.Colormap, 'LineStyle', 'None');
		dmax = max (split{n}(:));
		t = text (ax, -0.5 * ones (1, 10), ((1 : 10) - 0.5) / 10 * NC, sprintfc ('%.0f', (0 : 9) / 9 * dmax), 'HorizontalAlignment', 'center', 'Color', [1 1 1]);
		I = getframe (gcf);
		imwrite (I.cdata, sprintf ('dose_%i_%s.png', pidx, plan{pidx}.ionlist{n}), 'png');
	end
end

if length (plan) > 1
	Vt = 0;
	
	for pidx = 1 : length (plan)
		Vt = Vt + vtotal{pidx};
	end

	clf;

	view_panel = uipanel (figure (1));
	view_panel.FontSize = 14;
	view_panel.TitlePosition = 'centertop';

	max_dose = max (Vt(:));

	if max_dose < max_phantom
		Vt(1, 1, 1) = 100;
		max_dose = 100;
	end

	background = (phantom_rot{pidx} .* (oar_rot{pidx} < 0.1) / max_phantom + 1 * oar_rot{pidx} / max_oar) * max_dose * bg_intensity;

	h = volshow (Vt .* (Vt >= 0.01 * max_dose) + background .* (Vt < 0.01 * max_dose), 'BackgroundColor', [0 0 0], 'Renderer', 'MaximumIntensityProjection', 'CameraPosition', campos, 'CameraUpVector', [1 0 0], 'ColorMap', map, 'CameraTarget', camtarg, 'Parent', view_panel);
	ax = axes ('Parent', view_panel, 'Units','normalized', 'Position', [0.95 0 0.05 1], 'Visible', 'off', 'YLim', [0 NC], 'CLim', [0 NC], 'Colormap', cbarmap);
	p = patch ('Parent', ax, 'XData', repmat ([0; 1; 1; 0], 1, NC), 'YData', [0; 0; 1; 1] + (0 : (NC - 1)), 'FaceColor', 'flat', 'FaceVertexCData', ax.Colormap, 'LineStyle', 'None');
	dmax = max (Vt(:));
	t = text (ax, -0.5 * ones (1, 10), ((1 : 10) - 0.5) / 10 * NC, sprintfc ('%.0f', (0 : 9) / 9 * dmax), 'HorizontalAlignment', 'center', 'Color', [1 1 1]);
	I = getframe (gcf);
	imwrite (I.cdata, sprintf ('totaldose.png'), 'png');
end
