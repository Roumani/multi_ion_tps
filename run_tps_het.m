% Load ion_data if we haven't already done so - this saves a lot of time!

stderr = 2;

if ~exist ('ion_data', 'var')
	fprintf (stderr, 'Loading ion data from current working directory...\n'); 
	ion_data = load_ion_library;
	fprintf (stderr, 'Loading complete.\n')
else
	fprintf (stderr, 'Ion data already loaded; skipping loading.\n');
end

% target, tail, distal, peripheral, proximal, OAR downsample factors
reg_decimations = [1, 1, 25, 200, 25, 1];

% Relative weightings given to the respective regions
reg_weights = [1, 1, 1, 1, 0.01, 10];

% Combine both of the above to minimise the amount of changes needed to tps_3D_het
reg_decimations_weights = [reg_decimations; reg_weights];

grid_x = 5;
grid_y = 5;
grid_z = 5;

% Create target dose distribution - sphere centred on the target volume

xpix = 1:100;
ypix = 1:100;
zpix = 1:300;
centrex = 50;
centrey = 50;
centrez = 150;
tumour_r = 25;
head_r = 100;
skin_t = 2;
skull_t = 6;

dose = 100;

% Carbon only (use for testing purposes)
ion_data_subset = ion_data(4);
%ion_data_subset = ion_data([1:5]);
%head_r, skin_t, skull_t, tumour_r, dose, debug
%[phantom, targetdist] = create_spherical_phantom_target (xpix, ypix, zpix, centrex, centrey, centrez, head_r, skin_t, skull_t, tumour_r, dose);
[phantom, targetdist] = create_spherical_phantom_target (xpix, ypix, zpix, centrex, centrey, centrez, head_r, skin_t, skull_t, tumour_r, dose);
%[phantom, targetdist] = create_homogeneous_phantom_target (xpix, ypix, zpix, centrex, centrey, centrez, head_r, skin_t, skull_t, tumour_r, dose);

t0c = cputime;
t0r = tic;

[plan, D, A, Y] = tps3D_het (phantom, targetdist, ion_data_subset, reg_decimations_weights, grid_x, grid_y, grid_z, 'rect');

t1c = cputime;
elapsed = toc (t0r);

V = calculate_multi_ion_dose_distribution_het (plan, ion_data_subset);

% Compute residual
residual = sqrt (sum ((V(:) - targetdist(:)) .^ 2));
save residual.mat residual

%save V.mat V;
save V.mat V;
save -v7.3 plan.mat plan;
save targetdist.mat targetdist;

delta = t1c - t0c;
times = [delta; elapsed]

save -ascii times.dat times;
plan_and_result (plan, V, [centrex, centrey, centrez]);

%Converting TPS output into a macro file to be tested in Geant4  simulation model.
%plan_to_g4mac (plan, library)

fprintf (stderr, 'Elapsed CPU time: %.0f seconds\n', delta);
fprintf (stderr, 'Elapsed real time: %.0f seconds\n', elapsed);
