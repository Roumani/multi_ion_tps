% Load ion_data if we haven't already done so - this saves a lot of time!

stderr = 2;

if ~exist ('ion_data', 'var')
	fprintf (stderr, 'Loading ion data from current working directory...\n'); 
%	ion_data = load_ion_library;
	load simulated_ion_data_small_set.mat
	fprintf (stderr, 'Loading complete.\n')
else
	fprintf (stderr, 'Ion data already loaded; skipping loading.\n');
end

% Define the parameters of the this TPS
parameters;

% Combine both of the above to minimise the amount of changes needed to tps_3D_het
reg_decimations_weights = [reg_decimations; reg_weights];

[TBN, TBV] = get_labels ();

% Carbon only (use for testing purposes)
%ion_data_subset = ion_data([4]);
% Carbon-proton
% ion_data_subset = ion_data([1, 4]);
%ion_data_subset = ion_data([1:8]);
% Reverse order - proton last
%ion_data_subset = ion_data([1 : 8]);
%head_r, skin_t, skull_t, tumour_r, dose, debug

ion_data_subset

% Note: nominal dose of 1 Gy; this will be scaled later in the individual rotated fields
[phantom, target, oar, hypoxic] = create_spherical_phantom_target_oar ([xpix, ypix, zpix], [centrex, centrey, centrez], head_r, skin_t, skull_t, [tumourx, tumoury, tumourz], tumour_r, TBN.Tumour.value, [oarx, oary, oarz], oar_r, TBN.Brain.value, [hypoxicx, hypoxicy, hypoxicz], hypoxic_r, 1);

save phantom.mat phantom
save target.mat target
save oar.mat oar
save hypoxic.mat hypoxic

% Assume equal distribution of dose between angles...

Vt = 0;

t0c = cputime;
t0r = tic;

nfields = length (beam_angles);
dvfo = zeros (nfields, 1);

phvals = unique (phantom)';

for idx = 1 : nfields
	angle = -beam_angles (idx); % negative as we are rotating the phantom in the opposite direction

	fprintf (stderr, 'Rotating to field with %.2f degrees rotation\n', angle);

	rotmatx = [1 0 0 0; 0 cosd(angle) sind(angle) 0; 0 -sind(angle) cosd(angle) 0; 0 0 0 1];
		
	tmptarget = round (imwarp (target * 1, affine3d (rotmatx), 'bilinear'));
	tmpoar = round (imwarp (oar * 1, affine3d (rotmatx), 'bilinear'));
	tmphypoxic = round (imwarp (hypoxic * 1, affine3d (rotmatx), 'bilinear'));
		
	tmpphantom = ones (size (tmptarget)) * TBN.Air.value;
		
	for phval = phvals
		tmp = (phantom == phval) * 1; % binary 0 or 1 for that part of the phantom
		tmp = round (imwarp (tmp, affine3d (rotmatx), 'bilinear')); % should also be binary
		tmpphantom = (tmpphantom .* (tmp == 0)) + tmp * phval;
	end
		
	tmphypoxic = tmphypoxic & tmptarget;
	tmpoar = tmpoar & ~tmptarget;
		
	[xr, yr, zr] = size (tmpphantom);

	phantom_rot{idx} = tmpphantom (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
	target_rot{idx} = tmptarget (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
	oar_rot{idx} = tmpoar (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
	hypoxic_rot{idx} = tmphypoxic (round (xr / 2 - xrun / 2) + (1 : xrun), round (yr / 2 - yrun / 2) + (1 : yrun), round (zr / 2 - zrun / 2) + (1 : zrun));
end

[plan, D, A, Y] = tps3D_mf (phantom_rot, target_rot, ion_data_subset, reg_decimations_weights, grid_x, grid_y, grid_z, 'rect', oar_rot, hypoxic_rot, hypoxic_threshold_Z);

V = calculate_multi_ion_dose_distribution_mf (plan);

[vx, vy, vz] = size (V);
V = V (round (vx / 2 - xrun / 2) + (1 : xrun), round (vy / 2 - yrun / 2) + (1 : yrun), round (vz / 2 - zrun / 2) + (1 : zrun));

t1c = cputime;
elapsed = toc (t0r);

% Compute residual
%residual = sqrt (sum ((V(:) - target(:)) .^ 2));
%save residual.mat residual

save V.mat V;
save -v7.3 plan.mat plan;

delta = t1c - t0c;
times = [delta; elapsed]

save -ascii times.dat times;

[rms_error, D50, D90, PTV, V5, V10, V50] = plan_and_result (plan, V, [xrun / 2, yrun / 2, zrun / 2]);

save -ascii rms_error.dat rms_error;
D5090 = [D50 D90];
PTV_V5_V10_V50 = [PTV, V5, V10, V50]
save -ascii D5090.dat D5090;
save -ascii dose_volumes.dat PTV_V5_V10_V50;

%Converting TPS output into a macro file to be tested in Geant4  simulation model.
%plan_to_g4mac (plan, library)

fprintf (stderr, 'Elapsed CPU time: %.0f seconds\n', delta);
fprintf (stderr, 'Elapsed real time: %.0f seconds\n', elapsed);
