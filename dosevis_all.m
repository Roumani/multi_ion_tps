stderr = 2;
angle = -60;
map = jet (256);

depths = {'deep', 'central', 'shallow'};
%depths = {'central'};

combos = {'hyp_oar_all_ions', 'nohyp_nooar_all_ions', 'hyp_nooar_all_ions', 'nohyp_oar_all_ions'};
%combos = {'nohyp_nooar_all_ions'};

for d = 1 : length (depths)
	for c = 1 : length (combos)
		f = [depths{d} '/' combos{c}];
		
		if isdir (f)
			fprintf (stderr, 'Processing plan in %s\n', f);
			
			chdir (f);
			
			load plan.mat;

			plan.phantom_volume (plan.phantom_volume == 1) = 0;
%			plan.phantom_volume (plan.phantom_volume == 3) = 1;
%			plan.phantom_volume (plan.phantom_volume == 4) = 2;
%			plan.phantom_volume (plan.phantom_volume == 5) = 3;
			plan.phantom_volume (plan.phantom_volume == 7) = 0;

			dosevis (plan, angle, map);			
			
%			close all;
			
			chdir ('../..');
		end
	end
end
