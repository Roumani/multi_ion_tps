function [regions, idx, partitioned_idx] = partition_regions (plan, reg_decimations)

stderr = 2;
regdef;

regions = zeros (size (plan.target_volume));
xy_plane = squeeze (sum (plan.target_volume, 3));

% Generate subset of full resolution XY plane where we have some dose (0 or 1; 1 = member of set)
targ_xy_reg = (xy_plane > 0);
[targ_x, targ_y] = find (targ_xy_reg);
n_targ_points = length (targ_x);

targ_zrange = zeros (n_targ_points, 2);

% Tag target, proximal and distal regions (use labels defined above to label a volume called 'regions')
for r = 1 : n_targ_points
	nz = find (plan.target_volume (targ_x(r), targ_y(r), :));
	targ_zrange (r, 1) = min (nz);
	targ_zrange (r, 2) = max (nz);
	regions (targ_x(r), targ_y(r), targ_zrange (r, 1) : targ_zrange (r, 2)) = targ_val;
	regions (targ_x(r), targ_y(r), 1 : targ_zrange (r, 1) - 1) = proximal_val;
	regions (targ_x(r), targ_y(r), (targ_zrange (r, 2) + 1 + (0:9))) = tail_val;
	regions (targ_x(r), targ_y(r), targ_zrange (r, 2) + 11 : end) = distal_val;
end

% Generate subset of full resolution XY plane where we have zero dose (0 or 1; 1 = member of set)
peripheral_xy_reg = (xy_plane == 0);
[peripheral_x, peripheral_y] = find (peripheral_xy_reg);
n_peripheral_points = length (peripheral_x);

% Tag the peripheral-region (laterally peripheral to target)
for r = 1 : n_peripheral_points
	regions (peripheral_x (r), peripheral_y (r), :) = peripheral_val;
end

if isfield (plan, 'oar')
	fprintf (stderr, 'Adding OAR\n');
	regions (plan.oar == 1) = oar_val; % this overwrites any regions tagged as target/tail/distal/peripheral/proximal; OAR takes precedence
end

rflat = regions(:);

idx_target = find(rflat == targ_val);
idx_tail = find(rflat == tail_val);
idx_distal = find(rflat == distal_val);
idx_peripheral = find(rflat == peripheral_val);
idx_proximal = find(rflat == proximal_val);
idx_oar = find(rflat == oar_val);

partitioned_idx = {idx_target(1:reg_decimations(1):end); idx_tail(1:reg_decimations(2):end); idx_distal(1:reg_decimations(3):end); idx_peripheral(1:reg_decimations(4):end); idx_proximal(1:reg_decimations(5):end)};

if length (idx_oar)
	partitioned_idx{end + 1} = idx_oar(1:reg_decimations(6):end);
end

idx = [idx_target(1:reg_decimations(1):end); idx_tail(1:reg_decimations(2):end); idx_distal(1:reg_decimations(3):end); idx_peripheral(1:reg_decimations(4):end); idx_proximal(1:reg_decimations(5):end)];

if length (idx_oar)
	idx = [idx; idx_oar(1:reg_decimations(6):end)];
end
