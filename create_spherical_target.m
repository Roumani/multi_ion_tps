function spherical_target = create_spherical_target (xpix, ypix, zpix, centrex, centrey, centrez, radius, dose, debug)

%xpix and ypix are in the form of in vector [1:100];

[X, Y, Z] = meshgrid (xpix, ypix, zpix);

% Create a spherical target 

spherical_target = ((X-centrex).^2 + (Y-centrey).^2 + (Z-centrez).^2 <= radius^2) * dose;

%min (spherical_target(:))
%max (spherical_target(:))

% Multiply the spherical region of voxels by a value of a desired dose 

if nargin == 9 && debug
	figure(1)
	imagesc(squeeze(spherical_target(round (size (spherical_target, 1) / 2), :, :)))
	axis equal;
	colorbar;
	figure(2)
	imagesc(squeeze(spherical_target(:, round (size (spherical_target, 2) / 2), :)))
	axis equal;
	colorbar;
	figure(3)
	imagesc(squeeze(spherical_target(:, :, round (size (spherical_target, 3) / 2))))
	axis equal;
	colorbar;
end
