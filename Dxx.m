%
% Usage: [p, PTV, v5, v10, v50] = Dxx (percentage, target, V, phantom)
%
% percentage is threshold, e.g. D90 -> percentage = 90
% target = target volume; V = achieved dose distribution volume
% phantom = phantom volume (so we know where the body ends and air begins... dose to air shouldn't be counted)
% Returns Dxx as first output arg, planning target volume (PTV) as 2nd
% non-target volumes receiving 5, 10 and 50% of target dose in 3rd, 4th and 5th output args

function [percentagetotarget, PTV_mm3, V5_mm3, V10_mm3, V50_mm3] = Dxx (percentages, target, dosedist, phantom)

% Assume flat target dose...
desired_dose = max (target (:));

% Total planned volume in mm3^3 (assume vox vol = 1 mm3^3) with this desired dose
PTV_mm3 = sum (target (:) > 0);

targetdose = (dosedist .* (target > 0));

for k = 1 : length (percentages)
	Vachieved = sum (targetdose (:) >= (desired_dose * percentages(k) / 100));
	percentagetotarget(k) = Vachieved / PTV_mm3 * 100;
end

air = min (phantom (:));

% Non-target volume (in mm^3) receiving > 5%, 10% and 50% of target dose
V5_mm3 = sum ((targetdose (:) == 0) .* (phantom (:) > air) .* dosedist (:) > 0.05 * desired_dose);
V10_mm3 = sum ((targetdose (:) == 0) .* (phantom (:) > air) .*  dosedist (:) > 0.1 * desired_dose);
V50_mm3 = sum ((targetdose (:) == 0) .* (phantom (:) > air) .* dosedist (:) > 0.5 * desired_dose);
