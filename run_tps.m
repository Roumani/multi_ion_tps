% Load ion_data if we haven't already done so - this saves a lot of time!

stderr = 2;

if ~exist ('ion_data', 'var')
	fprintf (stderr, 'Loading ion data from current working directory...\n'); 
	ion_data = load_ion_library;
	fprintf (stderr, 'Loading complete.\n')
else
	fprintf (stderr, 'Ion data already loaded; skipping loading phase\n');
end

% target, distal, peripheral, proximal
reg_decimations = [1 50 50 -1];

grid_x = 5;
grid_y = 5;

% Create target dose distribution - sphere centred on the target volume

xpix = 1:100;
ypix = 1:100;
zpix = 1:300;
centrex = 50;
centrey = 50;
centrez = 150;
radius = 25;
dose = 100;

% Parameters for two spherical volumes
xpix = 1:100;
ypix = 1:100;
zpix = 1:300;
centrez_1 = 150;
centrez_2 = 130;
centrex_1 = 50;
centrex_2 = 70;
centrey_1 = 50;
centrey_2 = 60;
radius_1 = 25;
radius_2 = 30;
dose_1 = 100;
dose_2 = 70;


targ1 = create_spherical_target (xpix, ypix, zpix, centrex_1, centrey_1, centrez_1, radius_1, dose_1);
targ2 = create_spherical_target (xpix, ypix, zpix, centrex_2, centrey_2, centrez_2, radius_2, dose_2);

targetdist = targ1 + targ2;



%targetdist = create_spherical_target (xpix, ypix, zpix, centrex, centrey, centrez, radius, dose);
%targetdist = create_spherical_target (xpix_1, ypix_1, zpix_1, centrex_1, centrey_1, centrez_1, radius_1, dose_1, xpix_2, ypix_2, zpix_2, centrex_2, centrey_2, centrez_2, radius_2, dose_2);

[plan, D, A, Y] = tps3D (targetdist, ion_data, reg_decimations, grid_x, grid_y);

V = calculate_multi_ion_dose_distribution (plan, ion_data);

% Compute residual
residual = sqrt (sum ((V(:) - targetdist(:)) .^ 2));
save residual.mat residual

%save V.mat V;
save V.mat V;
save plan.mat plan;
save targetdist.mat targetdist;
plan_and_result (targetdist, V)

