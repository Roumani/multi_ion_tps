stderr = 2;
map = jet (256);

depths = {'deep', 'central', 'shallow'};

combos = {'no_hypoxia_no_oar', 'yes_hypoxia_no_oar', 'no_hypoxia_yes_oar', 'yes_hypoxia_yes_oar'};

%ion_list = {'H_He_O', 'Li_C_Fe', 'all_ions'};
ion_list = {'all_ions', 'C', 'C_O_Ne', 'Fe', 'H', 'H_C', 'He', 'He_O', 'H_He_O', 'Li', 'Li_C_Fe', 'Li_Ne', 'Ne', 'O', 'Si'};

for d = 1 : length (depths)
	for c = 1 : length (combos)
		for ions = 1 : length (ion_list)
			f = [combos{c} '/' depths{d} '/' ion_list{ions}];
		
			if isdir (f)
				fprintf (stderr, 'Processing plan in %s\n', f);
			
				chdir (f);

				if isfile ('plan.mat')
					load plan.mat;

					plan.phantom_volume (plan.phantom_volume == 1) = 0;
					plan.phantom_volume (plan.phantom_volume == 7) = 0;

					anim_split (plan, 'dose_%s.gif', map)
				else
					fprintf (stderr, 'No plan.mat found in %s\n', f);
				end

				chdir ('../../..');		
			end
		end
	end
end
