% Load ion_data if we haven't already done so - this saves a lot of time!

stderr = 2;

if ~exist ('ion_data', 'var')
	fprintf (stderr, 'Loading ion data from current working directory...\n'); 
	ion_data = load_ion_library;
	fprintf (stderr, 'Loading complete.\n')
else
	fprintf (stderr, 'Ion data already loaded; skipping loading.\n');
end

% target, distal, peripheral, proximal
reg_decimations = [1 -1 -1 -1];

grid_x = 4;
grid_y = 4;
grid_z = 4;

% Create target dose distribution - sphere centred on the target volume

xpix = 1:100;
ypix = 1:100;
zpix = 1:300;
centrex = 50;
centrey = 50;
centrez = 150;
tumour_r = 25;
head_r = 100;
skin_t = 2;
skull_t = 6;

dose = 100;

% H He Li C Fe

%ion_data_subset = ion_data([1:4 8]);
%only carbon

ion_data_subset = ion_data(4);

%head_r, skin_t, skull_t, tumour_r, dose, debug
[phantom, targetdist] = create_spherical_phantom_target (xpix, ypix, zpix, centrex, centrey, centrez, head_r, skin_t, skull_t, tumour_r, dose);
%[phantom, targetdist] = create_homogeneous_phantom_target (xpix, ypix, zpix, centrex, centrey, centrez, head_r, skin_t, skull_t, tumour_r, dose);

t0c = cputime;
t0r = tic;

[plan, D, A, Y] = tps3D_het (phantom, targetdist, ion_data_subset, reg_decimations, grid_x, grid_y, grid_z, 'rect');

t1c = cputime;
elapsed = toc (t0r);

V = calculate_multi_ion_dose_distribution_het (plan, ion_data_subset);

% Compute residual
residual = sqrt (sum ((V(:) - targetdist(:)) .^ 2));
save residual.mat residual

%save V.mat V;
save V.mat V;
save -v7.3 plan.mat plan;
save targetdist.mat targetdist;
plan_and_result (targetdist, V)

%Converting TPS output into a macro file to be tested in Geant4  simulation model.
%plan_to_g4mac (plan, library)

fprintf (stderr, 'Elapsed CPU time: %.0f seconds\n', t1c - t0c);
fprintf (stderr, 'Elapsed real time: %.0f seconds\n', elapsed);
