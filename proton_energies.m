function energies_MeV = proton_energies (depths_cm)

rho_pmma = 1.19000; % g/cm^3

%p = load ("proton_depth_dose.dat");
p = dlmread ('proton_depth.dat', " ", 7, 0);

[en, idx] = unique (p (:, 1));

en = p (:, 1);
depths = p (:, 2) / rho_pmma;

en = en (idx);
depths = depths (idx);

energies_MeV = interp1 (depths, en, depths_cm);
